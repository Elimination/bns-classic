﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.Common.Skills
{
    public enum SkillType
    {
        Single,
        Self,
        Direction,
        NoTarget,
    }
}
