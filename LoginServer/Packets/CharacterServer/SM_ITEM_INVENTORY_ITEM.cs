﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.CharacterServer;
using SagaBNS.LoginServer.Network.CharacterServer;

namespace SagaBNS.LoginServer.Packets.CharacterServer
{
    public class SM_ITEM_INVENTORY_ITEM : SagaBNS.Common.Packets.CharacterServer.SM_ITEM_INVENTORY_ITEM
    {
        public override Packet<CharacterPacketOpcode> New()
        {
            return new SM_ITEM_INVENTORY_ITEM();
        }

        public override void OnProcess(Session<CharacterPacketOpcode> client)
        {
            ((CharacterSession)client).OnItemInventoryItem(this);
        }
    }
}
