﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.CharacterServer;
using SagaBNS.LoginServer.Network.CharacterServer;

namespace SagaBNS.LoginServer.Packets.CharacterServer
{
    public class SM_CHAR_DELETE_RESULT : SagaBNS.Common.Packets.CharacterServer.SM_CHAR_DELETE_RESULT
    {
        public override Packet<CharacterPacketOpcode> New()
        {
            return new SM_CHAR_DELETE_RESULT();
        }

        public override void OnProcess(Session<CharacterPacketOpcode> client)
        {
            ((CharacterSession)client).OnCharDeleteResult(this);
        }
    }
}
