﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.LoginServer
{
    public class WorldInfo
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
