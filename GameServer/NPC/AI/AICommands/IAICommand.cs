﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagaBNS.Common.Actors;

namespace SagaBNS.GameServer.NPC.AI.AICommands
{
    public interface IAICommand
    {
        CommandTypes Type { get; }
        CommandStatus Status { get; }
        ActorExt Target { get; set; }
        void Update();
    }
}
