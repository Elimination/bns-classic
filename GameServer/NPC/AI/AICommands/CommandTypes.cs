﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.GameServer.NPC.AI.AICommands
{
    public enum CommandTypes
    {
        Move,
        Chase,
        Attack,
    }

    public enum CommandStatus
    {
        Running,
        Finished,
    }
}
