﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Map;
using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Map.PathFinding;

namespace SagaBNS.GameServer.NPC.AI.AICommands
{
    public class Attack : IAICommand
    {
        AI ai;
        ActorNPC self;
        ActorExt target;
        Map.Map map;
        DateTime nextTime = DateTime.Now;
        Skill curSkill;
        public Attack(AI ai, ActorNPC self, ActorExt target)
        {
            this.ai = ai;
            this.self = self;
            this.target = target;
            map = Map.MapManager.Instance.GetMap(self.MapInstanceID);
            ai.Period = 400;
            Init();
        }

        #region IAICommand 成员
        CommandStatus status = CommandStatus.Running;
        public CommandTypes Type
        {
            get { return CommandTypes.Attack; }
        }

        public CommandStatus Status
        {
            get { return status; }
        }

        public ActorExt Target
        {
            get
            {
                return target;
            }
            set
            {
                if (target != value)
                {
                    if (value == null)
                        Finish();
                    target = value;
                }
            }
        }

        void Init()
        {
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = self;
            evt.Target = self;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(PacketParameter.CombatStatus, 1);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, self, false);
            self.Status.IsInCombat = true;

            evt = new UpdateEvent();
            evt.Actor = self;
            evt.Target = self;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(PacketParameter.FaceTo, (long)target.ActorID);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, self, false);
        }

        void Finish()
        {
            if (self.Status.CastingSkill)
            {
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = self;
                evt.UpdateType = UpdateTypes.Skill;
                evt.Target = target;
                evt.Skill = curSkill;
                evt.SkillCastMode = SkillCastMode.Single;
                evt.SkillMode = SkillMode.End;

                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, self, false);
                self.Status.CastingSkill = false;
            }
            status = CommandStatus.Finished;
            {
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = self;
                evt.Target = self;
                evt.UpdateType = UpdateTypes.Actor;
                //evt.AddActorPara(PacketParameter.CombatStatus, 0);
                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, self, false);
                self.Status.IsInCombat = false;

                evt = new UpdateEvent();
                evt.Actor = self;
                evt.Target = self;
                evt.UpdateType = UpdateTypes.Actor;
                //evt.AddActorPara(PacketParameter.FaceTo, 0);
                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, self, false);
            }
            ai.DueTime = 100;
            ai.Period = 100;
            if (!ai.Activated && !ai.NoPlayer)
                ai.Activate();
        }

        public void Update()
        {
            if (self.DistanceToActor(target) <= ai.GetCurrentCastRange(target) || self.Status.CastingSkill)
            {
                DateTime now = DateTime.Now;
                if (!self.Status.CastingSkill)
                {
                    if (now > self.Status.SkillCooldownEnd && self.BaseData.Skill.Count > 0)
                    {
                        if (DateTime.Now < nextTime)
                            return;
                        nextTime = DateTime.Now.AddMilliseconds(self.BaseData.CombatThinkPeriod);

                        self.Dir = self.DirectionFromTarget(target);
                        curSkill = ChooseSkill(target);
                        if (curSkill == null)
                            return;

                        SkillArg arg = new SkillArg();
                        arg.Caster = self;
                        arg.Dir = self.Dir;
                        arg.Target = (curSkill.BaseData.SkillType == SkillType.NoTarget || curSkill.BaseData.SkillType == SkillType.Self) ? self : target;
                        arg.Skill = curSkill;
                        arg.SkillSession = (byte)Global.Random.Next(0, 255);
                        Skills.SkillManager.Instance.SkillCast(arg);
                    }
                }
            }
            else
                Finish();
        }

        Skill ChooseSkill(ActorExt target)
        {
            List<Skill> possibleSkills = ai.GetPossibleSkills(target);
            List<Skill> comboSkills = (from skill in possibleSkills
                                       where skill.BaseData.PreviousSkills.Count > 0 || skill.BaseData.RequiredCasterStance != SkillCastStances.None || skill.BaseData.RequiredTargetStance != SkillCastStances.None
                                       select skill).ToList();
            List<Skill> skills = comboSkills.Count > 0 ? comboSkills : possibleSkills;

            int idx = Global.Random.Next(1, skills.Count) - 1;
            if (idx >= skills.Count)
                return null;
            return skills[idx];
        }

        #endregion
    }
}
