﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Map;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets.GameServer;

namespace SagaBNS.GameServer.Services
{
    /// <summary>
    /// 线程安全的广播服务，将尽可能多的广播事件整合到一个封包
    /// </summary>
    public class BroadcastService : Task
    {
        GameSession client;
        ConcurrentQueue<UpdateEvent> events = new ConcurrentQueue<UpdateEvent>();
        ConcurrentDictionary<ulong, UpdateEvent> moveEvents = new ConcurrentDictionary<ulong, UpdateEvent>();
        ConcurrentQueue<UpdateEvent> appearEvents = new ConcurrentQueue<UpdateEvent>();
        ConcurrentQueue<UpdateEvent> disappearEvents = new ConcurrentQueue<UpdateEvent>();
        public BroadcastService(GameSession client)
            : base(0, 100, "BroadcastService")//500ms interval
        {
            this.client = client;
        }

        protected override void OnDeactivate()
        {
            events = null;
            appearEvents = null;
            disappearEvents = null;
            client = null;
        }

        public override void CallBack()
        {
            UpdateEvent current;
            List<UpdateEvent> shouldUpdate = new List<UpdateEvent>();
            List<Actor> shouldAppear = new List<Actor>();
            List<Actor> shouldDisappear = new List<Actor>();
            if (appearEvents == null || disappearEvents == null || events == null || client == null)
                return;
            int maxCount = events.Count;
            int count = 0;
            do
            {
                while (shouldAppear.Count < 20 && appearEvents.TryDequeue(out current))
                {
                    if (((ActorExt)current.Actor).Status.IsInCombat)
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.Actor = current.Actor;
                        evt.Target = current.Actor;
                        evt.UpdateType = UpdateTypes.Actor;
                        //evt.AddActorPara(PacketParameter.CombatStatus, 1);
                        EnqueueUpdateEvent(evt);
                    }
                    shouldAppear.Add(current.Actor);
                }
                while (shouldDisappear.Count < 20 && disappearEvents.TryDequeue(out current))
                {
                    shouldDisappear.Add(current.Actor);
                }
                while (shouldUpdate.Count < 20 && events.TryDequeue(out current))
                {
                    shouldUpdate.Add(current);
                    if (current.UpdateType == UpdateTypes.Movement)
                    {
                        UpdateEvent removed;
                        moveEvents.TryRemove(current.Actor.ActorID, out removed);
                    }
                    count++;
                }

                if (client != null)
                {
                    if (shouldUpdate.Count > 0)
                        client.SendActorUpdates(shouldUpdate);
                    if (shouldAppear.Count > 0 || shouldDisappear.Count > 0)
                        client.SendActorAppear(shouldAppear, shouldDisappear);
                }
                shouldUpdate.Clear();
                shouldAppear.Clear();
                shouldDisappear.Clear();
            } while (events != null && events.Count > 0 && count < maxCount);
        }

        public void EnqueueUpdateEvent(UpdateEvent evt)
        {
            switch (evt.UpdateType)
            {
                case UpdateTypes.Appear:
                    appearEvents.Enqueue(evt);
                    break;
                case UpdateTypes.Disappear:
                    disappearEvents.Enqueue(evt);
                    break;
                default:
                    if (evt.UpdateType == UpdateTypes.Movement)
                    {
                        UpdateEvent old;
                        if (moveEvents.TryGetValue(evt.Actor.ActorID, out old))
                        {
                            old.MoveArgument.BNSMoveType = evt.MoveArgument.BNSMoveType;
                            old.MoveArgument.DashID = evt.MoveArgument.DashID;
                            old.MoveArgument.DashUnknown = evt.MoveArgument.DashUnknown;
                            old.MoveArgument.Dir = evt.MoveArgument.Dir;
                            old.MoveArgument.MoveType = evt.MoveArgument.MoveType;
                            old.MoveArgument.PushBackSource = evt.MoveArgument.PushBackSource;
                            old.MoveArgument.SkillSession = evt.MoveArgument.SkillSession;
                            old.MoveArgument.Speed = evt.MoveArgument.Speed;
                            old.MoveArgument.X = evt.MoveArgument.X;
                            old.MoveArgument.Y = evt.MoveArgument.Y;
                            old.MoveArgument.Z = evt.MoveArgument.Z;
                            sbyte[] diffs;
                            while (evt.MoveArgument.PosDiffs.TryDequeue(out diffs))
                                old.MoveArgument.PosDiffs.Enqueue(diffs);
                            events.Enqueue(evt);
                        }
                        else
                        {
                            UpdateEvent newE = new UpdateEvent();
                            newE.Actor = evt.Actor;
                            newE.UpdateType = UpdateTypes.Movement;
                            newE.MoveArgument = new MoveArgument();
                            newE.MoveArgument.BNSMoveType = evt.MoveArgument.BNSMoveType;
                            newE.MoveArgument.DashID = evt.MoveArgument.DashID;
                            newE.MoveArgument.DashUnknown = evt.MoveArgument.DashUnknown;
                            newE.MoveArgument.Dir = evt.MoveArgument.Dir;
                            newE.MoveArgument.MoveType = evt.MoveArgument.MoveType;
                            newE.MoveArgument.PushBackSource = evt.MoveArgument.PushBackSource;
                            newE.MoveArgument.SkillSession = evt.MoveArgument.SkillSession;
                            newE.MoveArgument.Speed = evt.MoveArgument.Speed;
                            newE.MoveArgument.X = evt.MoveArgument.X;
                            newE.MoveArgument.Y = evt.MoveArgument.Y;
                            newE.MoveArgument.Z = evt.MoveArgument.Z;
                            sbyte[] diffs;
                            while (evt.MoveArgument.PosDiffs.TryDequeue(out diffs))
                                newE.MoveArgument.PosDiffs.Enqueue(diffs);
                            events.Enqueue(evt);
                        }
                    }
                    else
                        events.Enqueue(evt);                    
                    break;
            }
        }
    }
}
