﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Numerics;
using PhotoshopFile;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.VirtualFileSystem;
using SmartEngine.Core.Math;
using SagaBNS.GameServer.Manager;
using SagaBNS.GameServer.Map;
using SagaBNS.Common.Item;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Network;
using SagaBNS.GameServer.Item;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Effect;
using SagaBNS.GameServer.BaGua;
using SagaBNS.GameServer.NPC;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;
using SagaBNS.GameServer.Quests;
using SagaBNS.GameServer.Portal;
using SmartEngine.Network.Utils;
using SmartEngine.Core;
namespace SagaBNS.GameServer
{
    class GameServer
    {
        public static bool shutingdown = false;
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(ShutingDown);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        
            Logger.InitDefaultLogger("GameServer");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                          SagaBNS Game Server                ");
            Console.WriteLine("         (C)2010-2011 The BnS Emulator Project Development Team                ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Logger.ShowInfo("Version Informations:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("GameServer");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + VersionInformation.Version + "(" + VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Common Library");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SagaBNS.Common.VersionInformation.Version + "(" + SagaBNS.Common.VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("SmartEngine Network");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SmartEngine.Network.VersionInformation.Version + "(" + SmartEngine.Network.VersionInformation.ModifyDate + ")");

            Logger.ShowInfo("Initializing VirtualFileSystem...");
            VirtualFileSystemManager.Instance.Init(FileSystems.Real, ".");

            Logger.ShowInfo("Loading Configuration File...");

            Network<GamePacketOpcode>.SuppressUnknownPackets = false;// !Debugger.IsAttached;
            Map.Map.MAX_SIGHT_RANGE = 800;

            /*for (int i = 0; i < 200000; i++)
            {
                test t = new test(Global.Random.Next(100, 1000));
                t.Activate();
            }*/

            Configuration.Instance.Initialization("./Config/GameServer.xml");
            //Logger.CurrentLogger.LogLevel.Value = Configuration.Instance.LogLevel;

            SmartEngine.Network.Tasks.TaskManager.Instance.SetWorkerCount(4, 4);
            ItemFactory.Instance.Init("./DB/item_templates.xml", Encoding.UTF8);
            SkillFactory.Instance.Init("./DB/skill_templates.xml", Encoding.UTF8);
            Skills.SkillManager.Instance.Init();
            ExperienceManager.Instance.Init("./DB/level.xml", Encoding.UTF8);
            EffectManager.Instance.Init("./DB/effect.xml", Encoding.UTF8);
            BaGuaManager.Instance.Init("./DB/baguaset.xml", Encoding.UTF8);
            NPCDataFactory.Instance.Init("./DB/npc_templates.xml", Encoding.UTF8);
            NPCStoreFactory.Instance.Init(System.IO.Directory.GetFiles("./DB", "store*.xml"), Encoding.UTF8);
            NPCSpawnManager.Instance.Init("./DB/Spawns", Encoding.UTF8, true);
            CampfireSpawnManager.Instance.Init("./DB/campfires.xml", Encoding.UTF8);
            QuestManager.Instance.Init("./DB/Quests", Encoding.UTF8, true);
            FactionRelationFactory.Instance.Init("./DB/faction_relations.xml", Encoding.UTF8);
            
            Scripting.ScriptManager.Instance.LoadScript("./Scripts");

            PortalDataManager.Instance.Init("./DB/portal_templates.xml",Encoding.UTF8);
            MapManager.Instance.Init("./DB/map_templates.xml", Encoding.UTF8);
            MapManager.Instance.InitStandardMaps();

            ClientManager<SagaBNS.Common.Packets.AccountPacketOpcode>.InitialSendCompletionPort = 10;
            ClientManager<SagaBNS.Common.Packets.AccountPacketOpcode>.NewSendCompletionPortEveryBatch = 2;
            ClientManager<SagaBNS.Common.Packets.CharacterPacketOpcode>.InitialSendCompletionPort = 10;
            ClientManager<SagaBNS.Common.Packets.CharacterPacketOpcode>.NewSendCompletionPortEveryBatch = 2;

            Logger.ShowInfo(string.Format("Connecting account server at {0}:{1}", Configuration.Instance.AccountHost, Configuration.Instance.AccountPort));
            AccountSession.Instance.Host = Configuration.Instance.AccountHost;
            AccountSession.Instance.Port = Configuration.Instance.AccountPort;
            if (!AccountSession.Instance.Connect(5))
            {
                Logger.ShowError("Cannot connect to account server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            }
            while (AccountSession.Instance.State == SESSION_STATE.NOT_IDENTIFIED ||
                AccountSession.Instance.State == SESSION_STATE.CONNECTED ||
                AccountSession.Instance.State == SESSION_STATE.DISCONNECTED)
            {
                System.Threading.Thread.Sleep(100);
            }
            if (AccountSession.Instance.State == SESSION_STATE.REJECTED ||
                AccountSession.Instance.State == SESSION_STATE.FAILED)
            {
                if (AccountSession.Instance.State == SESSION_STATE.REJECTED)
                    Logger.ShowError("Account server refused login request, please check the password");
                Logger.ShowInfo("Shutting down in 20sec.");
                AccountSession.Instance.Network.Disconnect();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }
            Logger.ShowInfo("Login to account server successful");

            Logger.ShowInfo(string.Format("Connecting character server at {0}:{1}", Configuration.Instance.CharacterHost, Configuration.Instance.CharacterPort));
            CharacterSession.Instance.Host = Configuration.Instance.CharacterHost;
            CharacterSession.Instance.Port = Configuration.Instance.CharacterPort;
            if (!CharacterSession.Instance.Connect(5))
            {
                Logger.ShowError("Cannot connect to character server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            }
            while (CharacterSession.Instance.State == SESSION_STATE.NOT_IDENTIFIED ||
                CharacterSession.Instance.State == SESSION_STATE.CONNECTED ||
                CharacterSession.Instance.State == SESSION_STATE.DISCONNECTED)
            {
                System.Threading.Thread.Sleep(100);
            }
            if (CharacterSession.Instance.State == SESSION_STATE.REJECTED ||
                CharacterSession.Instance.State == SESSION_STATE.FAILED)
            {
                if (CharacterSession.Instance.State == SESSION_STATE.REJECTED)
                    Logger.ShowError("Character server refused login request, please check the password");
                Logger.ShowInfo("Shutting down in 20sec.");
                AccountSession.Instance.Network.Disconnect();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }
            Logger.ShowInfo("Login to character server successful");

            GameClientManager.Instance.Port = Configuration.Instance.Port;
            Encryption.Implementation = new SagaBNS.Common.Encryption.BNSAESEncryption();
            Network<GamePacketOpcode>.Implementation = new SagaBNS.Common.BNSGameNetwork<GamePacketOpcode>();
            if (!GameClientManager.Instance.Start())
            {
                Logger.ShowError("Cannot Listen on port:" + Configuration.Instance.Port);
                Logger.ShowError("Shutting down in 20sec.");
                GameClientManager.Instance.Stop();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }

            Logger.ShowInfo("Listening on port:" + GameClientManager.Instance.Port);
            Logger.ShowInfo("Accepting clients...");


            //处理Console命令
            while (true)
            {
                try
                {
                    string cmd = Console.ReadLine();
                    if (cmd == null)
                        break;
                    args = cmd.Split(' ');
                    switch (args[0].ToLower())
                    {
                        case "savehm":
                            SaveHeightMaps(true);
                            break;
                        case "filterhm":
                            FilterHeightMaps();
                            break;
                        case "printthreads":
                            ClientManager.PrintAllThreads();
                            break;
                        case "printband":
                            int sendTotal = 0;
                            int receiveTotal = 0;
                            Logger.ShowWarning("Bandwidth usage information:");
                            foreach (Session<GamePacketOpcode> i in GameClientManager.Instance.ValidClients)
                            {
                                try
                                {
                                    sendTotal += i.Network.UpStreamBand;
                                    receiveTotal += i.Network.DownStreamBand;
                                    Logger.ShowWarning(string.Format("Client:{0} Receive:{1:0.##}KB/s Send:{2:0.##}KB/s",
                                        i.ToString(),
                                        (float)i.Network.DownStreamBand / 1024,
                                        (float)i.Network.UpStreamBand / 1024));
                                }
                                catch { }

                            }
                            Logger.ShowWarning(string.Format("Total: Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                        (float)receiveTotal / 1024,
                                        (float)sendTotal / 1024));
                            Logger.ShowWarning(string.Format("Total Session:{0} OnlinePlayer:{1}", GameClientManager.Instance.Clients.Count, GameClientManager.Instance.ValidClients.Count));
                            break;
                        case "status":
                            {
                                Logger.ShowWarning(string.Format("TaskManager:\r\n       Total Tasks:{0} Execution/s:{1} Avg Exection Time:{2}ms \r\n       Avg Schedule Time:{3}ms", SmartEngine.Network.Tasks.TaskManager.Instance.RegisteredCount, SmartEngine.Network.Tasks.TaskManager.Instance.ExecutionCountPerMinute / 60, SmartEngine.Network.Tasks.TaskManager.Instance.AverageExecutionTime, SmartEngine.Network.Tasks.TaskManager.Instance.AverageScheduleTime));
                                Logger.ShowWarning(string.Format("BufferManager:\r\n       TotalAllocatedMemory:{0:0.00}MB FreeMemory:{1:0.##}MB", (float)SmartEngine.Network.Memory.BufferManager.Instance.TotalAllocatedMemory / 1024 / 1024, (float)SmartEngine.Network.Memory.BufferManager.Instance.FreeMemory / 1024 / 1024));
                                Logger.ShowWarning("Network Status:");
                                Logger.ShowWarning("GameServer:");
                                Logger.ShowWarning(string.Format("IOCP: \r\n       CurrentIOCPs:{0} Free IOCPs:{1}", GameClientManager.CurrentCompletionPort, GameClientManager.FreeCompletionPort));
                                Logger.ShowWarning("AccountSession:");
                                Logger.ShowWarning(string.Format("       Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                          (float)Network.AccountServer.AccountSession.Instance.Network.DownStreamBand / 1024,
                                          (float)Network.AccountServer.AccountSession.Instance.Network.UpStreamBand / 1024));
                                Logger.ShowWarning(string.Format("IOCP: \r\n       CurrentIOCPs:{0} Free IOCPs:{1}", ClientManager<SagaBNS.Common.Packets.AccountPacketOpcode>.CurrentCompletionPort, ClientManager<SagaBNS.Common.Packets.AccountPacketOpcode>.FreeCompletionPort));
                                Logger.ShowWarning("CharacterSession:");
                                Logger.ShowWarning(string.Format("       Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                         (float)Network.CharacterServer.CharacterSession.Instance.Network.DownStreamBand / 1024,
                                         (float)Network.CharacterServer.CharacterSession.Instance.Network.UpStreamBand / 1024));
                                Logger.ShowWarning(string.Format("IOCP: \r\n       CurrentIOCPs:{0} Free IOCPs:{1}", ClientManager<SagaBNS.Common.Packets.CharacterPacketOpcode>.CurrentCompletionPort, ClientManager<SagaBNS.Common.Packets.CharacterPacketOpcode>.FreeCompletionPort));
                                Logger.ShowWarning(string.Format("Total Session:{0} OnlinePlayer:{1}", GameClientManager.Instance.Clients.Count, GameClientManager.Instance.ValidClients.Count));
                                Logger.ShowWarning(string.Format("Total Managed Ram:{0:0.00} MB, Total Process Ram:{1:0.00}MB", (float)GC.GetTotalMemory(false) / 1024 / 1024, (float)Process.GetCurrentProcess().PrivateMemorySize64 / 1024 / 1024));
                            }
                            break;
                        case "toptasks":
                            {
                                Logger.ShowWarning("Top 10 Tasks:");
                                Dictionary<string, int> tasks = SmartEngine.Network.Tasks.TaskManager.Instance.TenTasksWithMostInstances;
                                foreach (KeyValuePair<string, int> i in tasks)
                                {
                                    Logger.ShowWarning(string.Format("{0} : {1} instances", i.Key, i.Value));
                                }
                            }
                            break;
                        case "mem":
                            {
                                Logger.ShowWarning(string.Format("Total Managed Ram:{0:0.00} MB, Total Process Ram:{1:0.00}MB", (float)GC.GetTotalMemory(false) / 1024 / 1024, (float)Process.GetCurrentProcess().PrivateMemorySize64/ 1024 / 1024));
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }
        }

        static void FilterHeightMaps()
        {
            foreach (Map.HeightMapBuilder i in Map.Map.heightmapBuilder.Values)
            {
                try
                {
                    i.Filter();
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }
        }

        static void SaveHeightMaps(bool saveBMP)
        {
            foreach (string i in Map.Map.heightmapBuilder.Keys)
            {
                System.IO.FileStream fs = null;
                try
                {
                    if (Map.Map.heightmapBuilder[i].MinX == 0 && Map.Map.heightmapBuilder[i].MaxX == 0)
                        continue;
                    fs = new System.IO.FileStream("DB/HeightMaps/" + i + ".builder", System.IO.FileMode.Create);
                    Map.Map.heightmapBuilder[i].ToStream(fs);
                    if (saveBMP)
                        Map.Map.heightmapBuilder[i].ToBMP("DB/HeightMaps/" + i + ".psd");
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }
            }

            Logger.ShowInfo("Heightmaps saved");
        }

        private static void ShutingDown(object sender, ConsoleCancelEventArgs args)
        {
            if (!shutingdown)
            {
                shutingdown = true;
                Logger.ShowInfo("Closing.....");
                GameClientManager.Instance.Stop();
                foreach (Network.Client.GameSession i in GameClientManager.Instance.Clients.ToArray())
                {
                    try
                    {
                        i.Network.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        Logger.ShowError(ex);
                    }
                }
                Logger.ShowInfo("Saving Heightmap information...");
                SaveHeightMaps(false);
                AccountSession.Instance.Disconnecting = true;
                AccountSession.Instance.Network.Disconnect(true);
                CharacterSession.Instance.Disconnecting = true;
                CharacterSession.Instance.Network.Disconnect(true);                
            }
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (!shutingdown)
            {
                shutingdown = true; 
                Exception ex = e.ExceptionObject as Exception;

                Logger.ShowError("Fatal: An unhandled exception is thrown, terminating...");
                Logger.ShowError("Error Message:" + ex.Message);
                Logger.ShowError("Call Stack:" + ex.StackTrace);
                GameClientManager.Instance.Stop();
                Logger.ShowError("Trying to save all player's data");
                foreach (Network.Client.GameSession i in GameClientManager.Instance.Clients.ToArray())
                {
                    try
                    {
                        i.Network.Disconnect();
                    }
                    catch (Exception exc)
                    {
                        Logger.ShowError(exc);
                    }
                }
                Logger.ShowInfo("Saving Heightmap information...");

                SaveHeightMaps(false);
                AccountSession.Instance.Disconnecting = true;
                AccountSession.Instance.Network.Disconnect(true);
                CharacterSession.Instance.Disconnecting = true;
                CharacterSession.Instance.Network.Disconnect(true);     
            }
        }
    }
}
