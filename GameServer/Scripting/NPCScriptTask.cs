﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartEngine.Network.Map;

using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Scripting
{
    public abstract class NPCScriptTask
    {
        public int Cycles { get; set; }

        public int EndCycles { get; set; }

        public NPCScriptHandler ScriptHandler { get; set; }

        public abstract void DoUpdate();

        protected Map.Map NPCMap
        {
            get
            {
                return MapManager.Instance.GetMap(ScriptHandler.NPC.MapInstanceID);
            }
        }
    }
}
