﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Scripting.ScriptTasks
{
    public class SpawnNPC : NPCScriptTask
    {
        ushort npcID, dir, motion;
        short x, y, z;
        int appearEffect;
        bool update = false;
        public SpawnNPC(ushort npcID, int appearEffect, short x, short y, short z, ushort dir, ushort motion)
        {
            Cycles = 2;
            this.npcID = npcID;
            this.appearEffect = appearEffect;
            this.x = x;
            this.y = y;
            this.z = z;
            this.dir = dir;
            this.motion = motion;
        }

        public override void DoUpdate()
        {
            if (!update)
            {
                Utils.SpawnNPC(NPCMap, npcID, appearEffect, x, y, z, dir, motion);
                update = true;
            }
        }
    }
}
