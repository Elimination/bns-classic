﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Scripting.ScriptTasks
{
    public class Move : NPCScriptTask
    {
        ushort dir, speed;
        short x, y, z;
        int dashID;
        short dashUnknown;
        Map.MoveType moveType;
        bool updated = false;
        public Move(short x, short y, short z, ushort dir, ushort speed, Map.MoveType moveType)
        {
            Cycles = 5;
            this.x = x;
            this.y = y;
            this.z = z;
            this.dir = dir;
            this.speed = speed;
            this.moveType = moveType;
        }

        public Move(short x, short y, short z, ushort dir, int dashID, short dashUnknown)
        {
            Cycles = 20;
            this.x = x;
            this.y = y;
            this.z = z;
            this.dir = dir;
            this.moveType = Map.MoveType.Dash;
            this.dashID = dashID;
            this.dashUnknown =dashUnknown;
        }

        public override void DoUpdate()
        {
            if (!updated)
            {
                MoveArgument arg = new MoveArgument();
                arg.X = x;
                arg.Y = y;
                arg.Z = z;
                arg.Dir = dir;
                arg.Speed = speed;
                arg.DashID = dashID;
                arg.DashUnknown =dashUnknown;
                arg.BNSMoveType = moveType;
                NPCMap.MoveActor(ScriptHandler.NPC, arg, false);
                updated = true;
            }
        }
    }
}
