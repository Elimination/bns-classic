﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Scripting.ScriptTasks
{
    public class Chat : NPCScriptTask
    {
        int dialogID;
        ushort dir;
        bool update = false;
        public Chat(int dialogID,ushort dir)
        {
            Cycles = 2;
            this.dialogID = dialogID;
            this.dir = dir;
        }

        public override void DoUpdate()
        {
            if (!update)
            {
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = ScriptHandler.NPC;
                evt.UpdateType = UpdateTypes.NPCTalk;
                evt.UserData = dialogID;
                NPCMap.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, ScriptHandler.NPC, false);
                update = true;
            }
        }
    }
}
