﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.GameServer.Scripting
{
    public static class Utils
    {
        public static void SpawnNPC(Map.Map map, ushort npcID, short x, short y, short z, ushort dir, ushort motion)
        {
            SpawnNPC(map, npcID, 0, x, y, z, dir, motion);
        }

        public static void SpawnNPC(Map.Map map, ushort npcID, int appearEffect, short x, short y, short z, ushort dir, ushort motion)
        {
            NPC.SpawnData spawn = new NPC.SpawnData();
            spawn.MapID = map.ID;
            spawn.IsMapObject = false;
            spawn.NpcID = npcID;
            spawn.Range = 0;
            spawn.Count = 1;
            spawn.X = x;
            spawn.Y = y;
            spawn.Z = z;
            spawn.Dir = dir;
            spawn.Delay = 0;
            spawn.Motion = motion;
            spawn.AppearEffect = appearEffect;
            spawn.DoSpawn(map);
        }
    }
}
