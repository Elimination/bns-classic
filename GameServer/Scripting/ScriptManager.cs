﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CSharp;
using System.IO;
using System.CodeDom.Compiler;
using System.Reflection;

using SmartEngine.Core;
using SmartEngine.Network.Utils;
using SmartEngine.Network;

namespace SagaBNS.GameServer.Scripting
{
    public class ScriptManager : Singleton<ScriptManager>
    {
        Dictionary<ushort, NPCScriptHandler> npcScripts = new Dictionary<ushort, NPCScriptHandler>();
        Dictionary<ulong, MapObjectScriptHandler> mapObjectScripts = new Dictionary<ulong, MapObjectScriptHandler>();
        string path;

        public Dictionary<ushort, NPCScriptHandler> NpcScripts { get { return npcScripts; } }
        public Dictionary<ulong, MapObjectScriptHandler> MapObjectScripts { get { return mapObjectScripts; } }
        public ScriptManager()
        {
        }

        public void LoadScript(string path)
        {
            Logger.ShowInfo("Loading uncompiled scripts");
            Dictionary<string, string> dic = new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } };
            CSharpCodeProvider provider = new CSharpCodeProvider(dic);
            int eventcount = 0;
            this.path = path;
            try
            {
                string[] files = Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories);
                Assembly newAssembly;
                int tmp;
                if (files.Length > 0)
                {
                    newAssembly = CompileScript(files, provider);
                    if (newAssembly != null)
                    {
                        tmp = LoadAssembly(newAssembly);
                        Logger.ShowInfo(string.Format("Containing {0} Scripts", tmp));
                        eventcount += tmp;
                    }
                }
                Logger.ShowInfo("Loading compiled scripts....");
                files = Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories);
                foreach (string i in files)
                {
                    newAssembly = Assembly.LoadFile(System.IO.Path.GetFullPath(i));
                    if (newAssembly != null)
                    {
                        tmp = LoadAssembly(newAssembly);
                        Logger.ShowInfo(string.Format("Loading {1}, Containing {0} Scripts", tmp, i));
                        eventcount += tmp;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ShowError(ex);
            }


            Logger.ShowInfo(string.Format("Totally {0} Scripts Added", eventcount));
        }

        private Assembly CompileScript(string[] Source, CodeDomProvider Provider)
        {
            //ICodeCompiler compiler = Provider.;
            CompilerParameters parms = new CompilerParameters();
            CompilerResults results;

            // Configure parameters
            parms.CompilerOptions = "/target:library /optimize";
            parms.GenerateExecutable = false;
            parms.GenerateInMemory = true;
            parms.IncludeDebugInformation = true;
            //parms.ReferencedAssemblies.Add(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Reference Assemblies\Microsoft\Framework\v3.5\System.Data.DataSetExtensions.dll");
            //parms.ReferencedAssemblies.Add(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Reference Assemblies\Microsoft\Framework\v3.5\System.Core.dll");
            //parms.ReferencedAssemblies.Add(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Reference Assemblies\Microsoft\Framework\v3.5\System.Xml.Linq.dll");
            parms.ReferencedAssemblies.Add("System.dll");
            parms.ReferencedAssemblies.Add("System.Core.dll");
            parms.ReferencedAssemblies.Add("SmartEngine.Network.dll");
            parms.ReferencedAssemblies.Add("Common.dll");
            parms.ReferencedAssemblies.Add("GameServer.exe");
            /*foreach (string i in Configuration.Instance.ScriptReference)
            {
                parms.ReferencedAssemblies.Add(i);
            }*/
            // Compile
            results = Provider.CompileAssemblyFromFile(parms, Source);
            if (results.Errors.HasErrors)
            {
                foreach (CompilerError error in results.Errors)
                {
                    if (!error.IsWarning)
                    {
                        Logger.ShowError("Compile Error:" + error.ErrorText);
                        Logger.ShowError("File:" + error.FileName + ":" + error.Line);
                    }
                }
                return null;
            }
            //get a hold of the actual assembly that was generated
            return results.CompiledAssembly;
        }

        private int LoadAssembly(Assembly newAssembly)
        {
            Module[] newScripts = newAssembly.GetModules();
            int count = 0;
            foreach (Module newScript in newScripts)
            {
                Type[] types = newScript.GetTypes();
                foreach (Type npcType in types)
                {
                    try
                    {
                        if (npcType.IsAbstract == true) continue;
                        if (npcType.GetCustomAttributes(false).Length > 0) continue;
                        if (npcType.IsSubclassOf(typeof(NPCScriptHandler)))
                        {
                            NPCScriptHandler newEvent;
                            try
                            {
                                newEvent = (NPCScriptHandler)Activator.CreateInstance(npcType);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                            if (!this.npcScripts.ContainsKey(newEvent.NpcID) && newEvent.NpcID != 0)
                            {
                                this.npcScripts.Add(newEvent.NpcID, newEvent);
                            }
                            else
                            {
                                if (newEvent.NpcID != 0)
                                    Logger.ShowWarning(string.Format("NpcID:{0} already exists, Class:{1} droped", newEvent.NpcID, npcType.FullName));
                            }
                        }
                        else if (npcType.IsSubclassOf(typeof(MapObjectScriptHandler)))
                        {
                            MapObjectScriptHandler newEvent;
                            try
                            {
                                newEvent = (MapObjectScriptHandler)Activator.CreateInstance(npcType);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                            ulong id = (ulong)newEvent.MapID << 32 | newEvent.ObjectID;
                            if (!this.mapObjectScripts.ContainsKey(id))
                            {
                                this.mapObjectScripts.Add(id, newEvent);
                            }
                            else
                            {
                                if (newEvent.ObjectID != 0 && newEvent.MapID != 0)
                                    Logger.ShowWarning(string.Format("MapID:{0} ObjectID:{1} already exists, Class:{2} droped", newEvent.MapID, newEvent.ObjectID, npcType.FullName));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.ShowError(ex);
                    }
                    count++;
                }
            }
            return count;
        }

    }
}
