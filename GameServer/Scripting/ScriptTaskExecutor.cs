﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

using SmartEngine.Network.Tasks;

namespace SagaBNS.GameServer.Scripting
{
    public class ScriptTaskExecutor : Task
    {
        ConcurrentQueue<NPCScriptTask> tasks;
        NPCScriptHandler npc;
        NPCScriptTask current;
        int cycle = 0;
        public ScriptTaskExecutor(NPCScriptHandler npc, ConcurrentQueue<NPCScriptTask> tasks)
            : base(0, 100, "ScriptTaskExecutor")
        {
            this.period = 100;
            this.tasks = tasks;
            this.npc = npc;
            tasks.TryDequeue(out current);
            current.EndCycles = cycle + current.Cycles;
            current.ScriptHandler = npc;
        }

        public void Append(ConcurrentQueue<NPCScriptTask> tasks)
        {
            if (tasks != null)
            {
                NPCScriptTask task;
                while (tasks.TryDequeue(out task))
                    this.tasks.Enqueue(task);
            }
        }

        public void Reset()
        {
            current = null;
            cycle = 0;
            if (tasks != null)
            {
                if (tasks.TryDequeue(out current))
                {
                    current.EndCycles = cycle + current.Cycles;
                    current.ScriptHandler = npc;
                }
                else
                    Deactivate();
            }
        }

        public override void CallBack()
        {
            if (current == null || tasks == null)
                Deactivate();
            current.DoUpdate();
            cycle++;
            if (current.EndCycles <= cycle)
            {
                if (tasks.TryDequeue(out current))
                {
                    current.EndCycles = cycle + current.Cycles;
                    current.ScriptHandler = npc;
                }
                else
                    Deactivate();
            }
        }
    }
}
