﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network.Map;
using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Quests;
using SagaBNS.GameServer.ActorEventHandlers;

namespace SagaBNS.GameServer.Scripting
{
    public abstract class NPCScriptHandler : NPCEventHandler
    {
        ScriptTaskExecutor taskExe;
        ConcurrentQueue<NPCScriptTask> tasks;
        //public ActorNPC NPC { get { return base.NPC; } set{base.NPC  }
        public abstract ushort NpcID { get; }

        public virtual void OnQuest(ActorPC pc, ushort questID, byte step, Quest quest)
        {
        }

        protected void NextStep(Quest q, byte status, short flag1, short flag2, short flag3)
        {
            q.Step++;
            q.NextStep = (byte)(q.Step + 1);
            q.StepStatus = status;
            q.Flag1 = flag1;
            q.Flag2 = flag2;
            q.Flag3 = flag3;

        }

        protected void ProcessQuest(ActorPC pc, byte step, Quest q)
        {
            Quests.QuestManager.Instance.ProcessQuest(pc, q.QuestID, step, q, NPC);
        }

        protected void UpdateQuest(ActorPC pc, Quest quest)
        {
            ((ActorEventHandlers.PCEventHandler)pc.EventHandler).Client.SendQuestUpdate(quest);
        }

        protected void CutScene(ActorPC pc, uint cutscene)
        {
            ((ActorEventHandlers.PCEventHandler)pc.EventHandler).Client.SendQuestCutScene(cutscene);
        }

        protected Quest CreateNewQuest(ActorPC pc, ushort questID)
        {
            Quest q = new Quest();
            q.QuestID = questID;
            if (pc.Quests.ContainsKey(questID))
                return null;
            else
            {
                pc.Quests[questID] = q;
                return q;
            }


        }

        protected void BeginTask()
        {
            if (tasks == null)
                tasks = new ConcurrentQueue<NPCScriptTask>();
        }

        protected void NPCChat(int dialogID, ushort dir)
        {
            tasks.Enqueue(new ScriptTasks.Chat(dialogID, dir));
        }

        protected void NPCChat(int dialogID, Actor faceTo)
        {
            tasks.Enqueue(new ScriptTasks.Chat(dialogID, NPC.DirectionFromTarget(faceTo)));
        }

        protected void Delay(int cycles)
        {
            tasks.Enqueue(new ScriptTasks.Delay(cycles));
        }

        protected void Move(short x, short y, short z, ushort dir, ushort speed)
        {
            Move(x, y, z, dir, speed, true);
        }

        protected void Move(short x, short y, short z, ushort dir, ushort speed, bool run)
        {
            tasks.Enqueue(new ScriptTasks.Move(x, y, z, dir, speed, run ? Map.MoveType.Run : Map.MoveType.Walk));
        }

        protected void Dash(short x, short y, short z, ushort dir,int dashID,short dashUnknown)
        {
            tasks.Enqueue(new ScriptTasks.Move(x, y, z, dir, dashID, dashUnknown));
        }

        protected void SpawnNPCTask(ushort npcID, short x, short y, short z, ushort dir, ushort motion)
        {
            tasks.Enqueue(new ScriptTasks.SpawnNPC(npcID, 0, x, y, z, dir, motion));
        }

        protected void SpawnNPCTask(ushort npcID, int appearEffect, short x, short y, short z, ushort dir, ushort motion)
        {
            tasks.Enqueue(new ScriptTasks.SpawnNPC(npcID, appearEffect, x, y, z, dir, motion));
        }

        protected ScriptTaskExecutor StartTask()
        {
            return StartTask(false);
        }

        protected ScriptTaskExecutor StartTask(bool overwrite)
        {
            if (taskExe != null)
            {
                if (overwrite && taskExe.Activated)
                {
                    Logger.ShowWarning(string.Format("NPC:{0} already has task started", NPC.NpcID));
                    taskExe = null;
                }
                else
                    taskExe.Append(tasks);
            }
            if (taskExe == null)
            {
                taskExe = new ScriptTaskExecutor(this, tasks);
                taskExe.Activate();
            }
            else
            {
                if (!taskExe.Activated)
                {
                    taskExe.Reset();
                    taskExe.Activate();
                }
            }
            tasks = null;
            return taskExe;
        }

        protected void SendNPCCommand(string command)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(NPC.MapInstanceID);
            List<Actor> list = map.Actors.Values.ToList();
            foreach (Actor i in list)
            {
                if (i == NPC)
                    continue;
                if (i.EventHandler.GetType().IsSubclassOf(typeof(NPCScriptHandler))) 
                    ((NPCScriptHandler)i.EventHandler).OnReceiveNPCCommand(NPC, command);
            }
        }

        protected void SpawnNPC(ushort npcID, short x, short y, short z, ushort dir, ushort motion)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(NPC.MapInstanceID);
            Utils.SpawnNPC(map, npcID, x, y, z, dir, motion);
        }

        protected void SpawnNPC(ushort npcID,int appearEffect, short x, short y, short z, ushort dir, ushort motion)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(NPC.MapInstanceID);
            Utils.SpawnNPC(map, npcID, appearEffect, x, y, z, dir, motion);
        }

        protected void Disappear()
        {
            Disappear(0);
        }

        protected void Disappear(int disappearEffect)
        {
            NPC.DisappearEffect = disappearEffect;
            Map.Map map = Map.MapManager.Instance.GetMap(NPC.MapInstanceID);
            map.DeleteActor(NPC);
        }

        public virtual void OnReceiveNPCCommand(ActorNPC npc, string command)
        {
        }
    }
}
