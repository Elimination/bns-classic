﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SagaBNS.Common.Actors;
using SagaBNS.Common.Item;
using SagaBNS.GameServer.Item;
using SagaBNS.Common.Quests;

namespace SagaBNS.GameServer.Scripting
{
    public abstract class MapObjectScriptHandler
    {
        public virtual void OnMapObjectNPCCommnd(ActorPC npc, SmartEngine.Network.Map.Map<Map.MapEvents> map)
        {
        }
        public abstract void OnOperate(ActorPC pc, Map.Map map);
        public abstract uint MapID { get; }
        public abstract uint ObjectID { get; }

        protected void GiveItem(ActorPC pc, uint itemID)
        {
            ((ActorEventHandlers.PCEventHandler)pc.EventHandler).Client.AddItem(itemID);
        }

        protected void UpdateQuest(ActorPC pc, Quest quest)
        {
            ((ActorEventHandlers.PCEventHandler)pc.EventHandler).Client.SendQuestUpdate(quest);
        }

        protected void SpawnNPC(Map.Map map, ushort npcID, short x, short y, short z, ushort dir, ushort motion)
        {
            Utils.SpawnNPC(map, npcID, x, y, z, dir, motion);
        }

        protected Quest CreateNewQuest(ActorPC pc, ushort questID)
        {
            Quest q = new Quest();
            q.QuestID = questID;
            if (pc.Quests.ContainsKey(questID))
                return null;
            else
            {
                pc.Quests[questID] = q;
                return q;
            }
        }        
    }
}
