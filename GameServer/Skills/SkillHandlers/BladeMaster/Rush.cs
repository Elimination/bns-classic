﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.BladeMaster
{
    public class Rush : DefaultAttack
    {
        public Rush()
            : base(true)
        {
        }
        #region ISkillHandler 成员

        public override void HandleOnSkillCasting(SkillArg arg)
        {
            Map.Map map = MapManager.Instance.GetMap(arg.Caster.MapInstanceID);

            Map.MoveArgument argu = new Map.MoveArgument();
            argu.BNSMoveType = Map.MoveType.StepForward;
            int distance = arg.Caster.DistanceToActor(arg.Target);
            int forward = distance < 40 ? 0 : distance - 20;
            argu.SkillSession = arg.SkillSession;
            float deltaX = (float)(arg.Target.X - arg.Caster.X) / distance;
            float deltaY = (float)(arg.Target.Y - arg.Caster.Y) / distance;

            argu.X = arg.Caster.X + (int)(deltaX * forward);
            argu.Y = arg.Caster.Y + (int)(deltaY * forward);
            argu.Z = (short)arg.Target.Z;
            argu.Dir = arg.Caster.Dir;
            map.MoveActor(arg.Caster, argu, true);
        }
        #endregion
    }
}
