﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Common.Additions
{
    public abstract class BonusAddition : Buff
    {
        public short AccumulateCount { get; set; }
        public uint BonusAdditionID { get; set; }
        public BonusAddition(ActorExt actor, string name, int duration)
            : base(actor, name, duration)
        {
        }
    }
}
