﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Common.Additions
{
    public class MovementLock : Buff
    {
        GameSession client;
        public MovementLock(GameSession client,int lockDuration)
            : base(client.Character, "MovementLock", lockDuration)
        {
            this.client = client;
            Task task;
            if (client.Character.Tasks.TryGetValue("MovementLock", out task))
                task.Deactivate();
            client.Character.Tasks["MovementLock"] = this;
            this.OnAdditionStart += new StartEventHandler(MovementLock_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(MovementLock_OnAdditionEnd);
        }

        void MovementLock_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            Task removed;
            client.Character.Tasks.TryRemove("MovementLock", out removed);
            client.SendPlayerMovementLock(false);
        }

        void MovementLock_OnAdditionStart(Actor actor, Buff skill)
        {
            client.SendPlayerMovementLock(true);
        }        
    }
}
