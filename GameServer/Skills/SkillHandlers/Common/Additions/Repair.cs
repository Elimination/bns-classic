﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Item;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Common.Additions
{
    public class Repair : Buff
    {
        SkillArg arg;
        Dictionary<SagaBNS.Common.Item.Item, ushort> items = new Dictionary<SagaBNS.Common.Item.Item, ushort>();
        public Repair(SkillArg arg,Dictionary<SagaBNS.Common.Item.Item, ushort> items)
            : base(arg.Caster, "Repair", 7500)
        {
            this.arg = arg;
            this.items = items;
            this.OnAdditionStart += new StartEventHandler(Repair_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(Repair_OnAdditionEnd);
        }

        void Repair_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            SagaBNS.GameServer.Network.Client.GameSession session = SagaBNS.GameServer.Manager.GameClientManager.Instance.FindClient(actor.Name);
            Task removed;
            ((ActorExt)actor).Tasks.TryRemove("Repair", out removed);

            foreach (KeyValuePair<SagaBNS.Common.Item.Item, ushort> i in items)
            {
                session.RemoveItemSlot(i.Key.SlotID, i.Value);
            }

            //TODO: Set Durability to Max for equipted weapon

            UpdateEvent evt = new UpdateEvent();
            evt.UpdateType = UpdateTypes.Repair;
            evt.Actor = actor;
            evt.Target = arg.Target;
            evt.AdditionCount = 1;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }

        void Repair_OnAdditionStart(Actor actor, Buff skill)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            UpdateEvent evt = new UpdateEvent();
            evt.UpdateType = UpdateTypes.Repair;
            evt.Actor = actor;
            evt.Target = arg.Target;
            evt.AdditionCount = 0;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }
    }
}
