﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Common.Additions
{
    public class ActorDown : Buff
    {
        SkillArg arg;
        ActorExt target;
        uint additionID;
        int damage;
        public ActorDown(SkillArg arg, ActorExt target, uint additionID, int damage = 0, int duration = 5000)
            : base(target, "ActorDown", duration)
        {
            this.arg = arg;
            this.target = target;
            this.additionID = additionID;
            this.damage = damage;
            this.OnAdditionStart += new StartEventHandler(ActorDown_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(ActorDown_OnAdditionEnd);
        }

        void ActorDown_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            target.Status.Down = false;
            Task removed;
            target.Tasks.TryRemove("ActorDown", out removed);
            target.Status.StanceFlag1.SetValue(StanceU1.Down1, false);
            target.Status.StanceFlag1.SetValue(StanceU1.NoMove, false);
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = arg.Caster;
            evt.AdditionID = additionID;
            evt.Target = target;
            evt.Skill = arg.Skill;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.SkillSession = arg.SkillSession;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Unk7A, target.Status.StanceFlag1.Value);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkDF, 0);
            evt.UserData = new byte[] { 9, 1, 0 };
            MapManager.Instance.GetMap(actor.MapInstanceID).SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, target, true);
        }

        unsafe void ActorDown_OnAdditionStart(Actor actor, Buff skill)
        {
            if (!target.Status.Down)
            {
                target.Status.Down = true;
                target.Status.StanceFlag1.SetValue(StanceU1.Down1, true);
                target.Status.StanceFlag1.SetValue(StanceU1.NoMove, true);
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = arg.Caster;
                evt.AdditionID = additionID;
                evt.Target = target;
                evt.Skill = arg.Skill;
                evt.SkillSession = arg.SkillSession;
                evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Activate;
                evt.UpdateType = UpdateTypes.Actor;
                //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Unk7A, target.Status.StanceFlag1.Value);
                //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkDF, 1);
                byte[] buf = new byte[7];
                fixed (byte* res = buf)
                {
                    res[0] = 3;
                    *(int*)&res[2] = damage;
                }
                evt.UserData = buf;
                MapManager.Instance.GetMap(actor.MapInstanceID).SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, target, true);
            }
        }

        
    }
}
