﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.KungfuMaster
{
    public class Armbar : DefaultAttack
    {
        #region ISkillHandler 成员

        public override void OnAfterSkillCast(SkillArg arg)
        {
            base.OnAfterSkillCast(arg);
            Task task;
            if (arg.Caster.Tasks.TryGetValue("ActorTakeDown", out task))
            {
                Buff buf = task as Buff;
                buf.EndTime = DateTime.Now.AddMilliseconds(1500);
                //buf.Deactivate();
            }
            if (arg.Target.Tasks.TryGetValue("ActorDown", out task))
                task.Deactivate();
        }

        #endregion
    }
}
