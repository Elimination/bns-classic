﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.KungfuMaster
{
    public class Counter : DefaultAttack
    {
        #region ISkillHandler 成员

        public override void HandleSkillActivate(SkillArg arg)
        {
            base.HandleSkillActivate(arg);
            foreach (SkillAffectedActor i in arg.AffectedActors)
            {
                if (i.Result == SkillAttackResult.Normal || i.Result == SkillAttackResult.Critical)
                {
                    Common.Additions.Stun stun = new Common.Additions.Stun(arg, i.Target, 3000);
                    stun.Activate();
                }
            }
            Additions.CounterSelf counter = new Additions.CounterSelf(arg, 200);
            counter.Activate();
        }

        #endregion
    }
}
