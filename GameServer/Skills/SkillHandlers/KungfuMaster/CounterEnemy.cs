﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.KungfuMaster
{
    public class CounterEnemy : ISkillHandler
    {
        #region ISkillHandler 成员

        public void HandleOnSkillCasting(SkillArg arg)
        {
            
        }

        public void HandleOnSkillCastFinish(SkillArg arg)
        {
            
        }

        public void HandleSkillActivate(SkillArg arg)
        {
            Additions.CounterEnemy buff = new Additions.CounterEnemy(arg);
            buff.Activate();
        }

        public void OnAfterSkillCast(SkillArg arg)
        {
            
        }

        #endregion
    }
}
