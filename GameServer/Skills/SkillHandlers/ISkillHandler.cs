﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SagaBNS.GameServer.Skills;
using SagaBNS.Common.Skills;

namespace SagaBNS.GameServer.Skills.SkillHandlers
{
    public interface ISkillHandler
    {
        void HandleOnSkillCasting(SkillArg arg);

        void HandleOnSkillCastFinish(SkillArg arg);

        void HandleSkillActivate(SkillArg arg);

        void OnAfterSkillCast(SkillArg arg);
    }
}
