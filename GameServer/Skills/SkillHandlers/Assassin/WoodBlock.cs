﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.Assassin
{
    public class WoodBlock : ISkillHandler
    {
        public WoodBlock()
        {
            
        }

        #region ISkillHandler 成员

        public void HandleOnSkillCasting(SkillArg arg)
        {
            
        }

        public void HandleOnSkillCastFinish(SkillArg arg)
        {
            
        }

        public void HandleSkillActivate(SkillArg arg)
        {
            Additions.WoodBlock buff = new Additions.WoodBlock(arg);
            buff.Activate();
        }

        public void OnAfterSkillCast(SkillArg arg)
        {
        }
        #endregion
    }
}
