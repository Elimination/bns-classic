﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.Assassin
{
    public class SpineStab : DefaultAttack
    {
        #region ISkillHandler 成员

        public override void  HandleSkillActivate(SkillArg arg)
        {
            if (Math.Abs(arg.Caster.Dir - arg.Target.Dir) <= 90)
                base.HandleSkillActivate(arg);
        }

        #endregion
    }
}
