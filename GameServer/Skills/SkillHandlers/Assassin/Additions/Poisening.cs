﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Assassin.Additions
{
    public class Poisening : Buff
    {
        SkillArg arg;
        public Poisening(SkillArg arg)
            : base(arg.Caster, "Poisening", 30000)
        {
            this.arg = arg;
            Task removed;
            if (arg.Caster.Tasks.TryRemove("Poisening", out removed))
                removed.Deactivate();
            arg.Caster.Tasks["Poisening"] = this;
            this.OnAdditionStart += new StartEventHandler(Poisening_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(Poisening_OnAdditionEnd);
        }

        void Poisening_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            Task removed;
            arg.Caster.Tasks.TryRemove("Poisening", out removed);
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            UpdateEvent evt = UpdateEvent.NewActorAdditionEvent(arg.Caster, arg.Caster, arg.SkillSession, 3, 15000016, UpdateEvent.ExtraUpdateModes.Cancel);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF9, 0);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = UpdateEvent.NewActorAdditionExtEvent(arg.Caster, arg.SkillSession, 3, 15000016, 3000, UpdateEvent.ExtraUpdateModes.Cancel);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }

        void Poisening_OnAdditionStart(Actor actor, Buff skill)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            UpdateEvent evt = UpdateEvent.NewActorAdditionEvent(arg.Caster, arg.Caster, arg.SkillSession, 3, 15000016, UpdateEvent.ExtraUpdateModes.Activate);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF9, 1);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = UpdateEvent.NewActorAdditionExtEvent(arg.Caster, arg.SkillSession, 3, 15000016, 3000, UpdateEvent.ExtraUpdateModes.Activate);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }
    }
}
