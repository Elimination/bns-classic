﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.Assassin.Additions
{
    public class Stealth : Buff
    {
        SkillArg arg;
        public Stealth(SkillArg arg)
            : base(arg.Caster, "Stealth", 6000)
        {
            this.arg = arg;
            this.OnAdditionStart += new StartEventHandler(Stealth_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(Stealth_OnAdditionEnd);
        }

        void Stealth_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            arg.Caster.Status.Stealth = false;
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            Task removed;
            ((ActorExt)actor).Tasks.TryRemove("Stealth", out removed);
            arg.Caster.Status.StanceFlag1.SetValue(StanceU1.Dash, false);
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.SkillSession = arg.SkillSession;
            evt.AdditionID = 15119010;
            evt.AdditionSession = 5;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Speed, 62);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Unk7A, arg.Caster.Status.StanceFlag1.Value);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF4, 0);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF0, 0);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.AdditionSession = 5;
            evt.AdditionID = 15119010;
            evt.RestTime = 6000;
            evt.SkillSession = arg.SkillSession;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.UpdateType = UpdateTypes.ActorExtension;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.SkillSession = 255;
            evt.AdditionID = 15000025;
            evt.AdditionSession = 8194;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkEB, 0);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.AdditionSession = 8194;
            evt.AdditionID = 15000025;
            evt.RestTime = 0;
            evt.SkillSession = arg.SkillSession;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.UpdateType = UpdateTypes.ActorExtension;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            arg.Caster.ChangeStance(Stances.Assassin_Normal, arg.SkillSession, 15001011);            
        }

        void Stealth_OnAdditionStart(Actor actor, Buff skill)
        {
            arg.Caster.Status.Stealth = true;
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            arg.Caster.Status.StanceFlag1.SetValue(StanceU1.Dash, true);
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.SkillSession = arg.SkillSession;
            evt.AdditionID = 15119010;
            evt.AdditionSession = 5;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Activate;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Speed, 124);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.Unk7A, arg.Caster.Status.StanceFlag1.Value);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF4, 1);
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkF0, 1);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.AdditionSession = 5;
            evt.AdditionID = 15119010;
            evt.RestTime = 6000;
            evt.SkillSession = arg.SkillSession;
            evt.UpdateType = UpdateTypes.ActorExtension;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.SkillSession = 255;
            evt.AdditionID = 15000025;
            evt.AdditionSession = 8194;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Activate;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(SagaBNS.Common.Packets.GameServer.PacketParameter.UnkEB , 1);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            evt = new UpdateEvent();
            evt.Actor = actor;
            evt.Target = actor;
            evt.AdditionSession = 8194;
            evt.AdditionID = 15000025;
            evt.RestTime = 0;
            evt.SkillSession = arg.SkillSession;
            evt.UpdateType = UpdateTypes.ActorExtension;
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);

            arg.Caster.ChangeStance(Stances.Assassin_Stealth, arg.SkillSession, 15001011, 10);
        }
    }
}
