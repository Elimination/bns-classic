﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.Assassin
{
    public class AssassinAttack : DefaultAttack
    {
        public AssassinAttack()
        {
        }
        public AssassinAttack(bool addmana)
            : base(addmana)
        {
        }
        #region ISkillHandler 成员

        public override void  HandleSkillActivate(SkillArg arg)
        {
            base.HandleSkillActivate(arg);
            if (arg.Caster.Tasks.ContainsKey("Poisening"))
            {
                foreach (SkillAffectedActor i in arg.AffectedActors)
                {
                    if (i.Result.IsHit())
                    {
                        Additions.PoisenCharge poisen = new Additions.PoisenCharge(arg, i.Target);
                        poisen.Activate();
                    }
                }
            }
        }

        #endregion
    }
}
