﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;
namespace SagaBNS.GameServer.Skills.SkillHandlers.ForceMaster.Additions
{
    public class FrosenSelf : Buff
    {
        SkillArg arg;
        public FrosenSelf(SkillArg arg)
            : base(arg.Caster, "FrosenSelf", 10000)
        {
            this.arg = arg;
            Task task;
            if (arg.Caster.Tasks.TryGetValue("FrosenSelf", out task))
            {
                task.Deactivate();
            }
            arg.Caster.Tasks["FrosenSelf"] = this;
            this.OnAdditionStart += new StartEventHandler(FrosenSelf_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(FrosenSelf_OnAdditionEnd);
        }

        void FrosenSelf_OnAdditionEnd(Actor actor, Buff skill, bool cancel)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            Task removed;
            ((ActorExt)actor).Tasks.TryRemove("FrosenSelf", out removed);
            UpdateEvent evt = UpdateEvent.NewActorAdditionExtEvent(arg.Caster, arg.SkillSession, 10, 12000040, 10000, UpdateEvent.ExtraUpdateModes.Cancel);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }

        void FrosenSelf_OnAdditionStart(Actor actor, Buff skill)
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);

            UpdateEvent evt = UpdateEvent.NewActorAdditionExtEvent(arg.Caster, arg.SkillSession, 10, 12000040, 10000, UpdateEvent.ExtraUpdateModes.Activate);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }
    }
}
