﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.ForceMaster
{
    public class MagneticCatch : DefaultAttack
    {

        uint additionID;
        public MagneticCatch(uint additionID)
        {
            this.additionID = additionID;
        }
        #region ISkillHandler 成员

        public override void HandleSkillActivate(SkillArg arg)
        {
            SkillManager.Instance.DoAttack(arg);
            List<SkillAffectedActor> affected = arg.AffectedActors;
            foreach (SkillAffectedActor i in affected)
            {
                SkillAttackResult res = i.Result;
                if (res != SkillAttackResult.Avoid && res != SkillAttackResult.Miss && res != SkillAttackResult.Parry && res != SkillAttackResult.TotalParry)
                {
                    if (i.Target.Tasks.ContainsKey("ActorCatch"))
                    {
                        Buff buff = i.Target.Tasks["ActorCatch"] as Buff;
                        buff.Deactivate();
                    }

                    Common.Additions.ActorCatch add = new Common.Additions.ActorCatch(arg, i.Target, additionID, i.Damage, 5000);

                    i.Target.Tasks["ActorCatch"] = add;
                    add.Activate();
                }
            }
        }

        #endregion
    }
}
