﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Skills.SkillHandlers.ForceMaster
{
    public class RapidCooling : DefaultAttack
    {
        public RapidCooling()
            : base(false)
        {
        }
        #region ISkillHandler 成员

        public override void HandleSkillActivate(SkillArg arg)
        {
            base.HandleSkillActivate(arg);
            for (int idx = 0; idx < arg.AffectedActors.Count; idx++)
            {
                SkillAffectedActor i = arg.AffectedActors[idx];
                if (i.Result.IsHit())
                {
                    Additions.Frost frost = new Additions.Frost(arg, i.Target);
                    frost.Activate();
                }
            }

        }

        #endregion
    }
}
