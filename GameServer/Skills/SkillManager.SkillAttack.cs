﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SmartEngine.Core;
using SmartEngine.Core.Math;
using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Tasks.Actor;
using SagaBNS.GameServer.Map;

using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;

namespace SagaBNS.GameServer.Skills
{
    public partial class SkillManager : Singleton<SkillManager>
    {
        public void DoAttack(SkillArg arg)
        {
            List<SkillAffectedActor> affected = arg.AffectedActors;
            List<ActorExt> targets = new List<ActorExt>();
            #region Target Selection
            if (!arg.Caster.Status.Dead && !arg.Caster.Status.Down && !arg.Caster.Status.Invincible)
            {
                if (arg.CastMode == SkillCastMode.Coordinate)
                {
                    Map.Map map = MapManager.Instance.GetMap(arg.Caster.MapInstanceID);
                    List<Actor> list = map.GetActorsAroundArea(arg.X, arg.Y, arg.Z, arg.Skill.BaseData.NoTargetRange > 0 ? arg.Skill.BaseData.NoTargetRange : arg.Skill.BaseData.CastRangeMax);
                    foreach (Actor i in list)
                    {
                        if (NPC.FactionRelationFactory.Instance[arg.Caster.Faction][((ActorExt)i).Faction] != Relations.Friendly)
                            targets.Add((ActorExt)i);
                    }
                }
                else
                {
                    switch (arg.Skill.BaseData.SkillType)
                    {
                        case SkillType.Single:
                            if (!arg.Target.Status.Dead || arg.Target.Status.Recovering)
                            {
                                int dir = arg.Dir.DirectionRelativeToTarget(arg.Caster.X, arg.Caster.Y, arg.Target.X, arg.Target.Y);
                                if (dir <= arg.Skill.BaseData.NoTargetAngle)
                                    targets.Add(arg.Target);
                            }
                            break;
                        case SkillType.NoTarget:
                            {
                                Map.Map map = MapManager.Instance.GetMap(arg.Caster.MapInstanceID);
                                List<Actor> list = map.GetActorsAroundActor(arg.Caster, arg.Skill.BaseData.NoTargetRange > 0 ? arg.Skill.BaseData.NoTargetRange : arg.Skill.BaseData.CastRangeMax, false);
                                foreach (Actor i in list)
                                {
                                    if (i.ActorType == ActorType.CORPSE || i.ActorType == ActorType.ITEM)
                                        continue;
                                    if (arg.Skill.BaseData.NoTargetType == NoTargetTypes.Angular)
                                    {
                                        if (arg.Dir.DirectionRelativeToTarget(arg.Caster.X, arg.Caster.Y, i.X, i.Y) <= arg.Skill.BaseData.NoTargetAngle && NPC.FactionRelationFactory.Instance[arg.Caster.Faction][((ActorExt)i).Faction] != Relations.Friendly)
                                            targets.Add((ActorExt)i);
                                    }
                                    else
                                    {
                                        float degree = arg.Caster.DirectionRelativeToTarget(i);
                                        float distance=arg.Caster.DistanceToActor(i);
                                        Degree deg = (Degree)degree;
                                        float width = (float)(distance * Math.Sin(deg.InRadians()));
                                        float length = (float)(distance * Math.Cos(deg.InRadians()));
                                        if (width <= arg.Skill.BaseData.NoTargetWidth && NPC.FactionRelationFactory.Instance[arg.Caster.Faction][((ActorExt)i).Faction] != Relations.Friendly && degree < 90)
                                            targets.Add((ActorExt)i);
                                    }
                                }
                            }
                            break;
                        case SkillType.Self:
                            //Logger.ShowDebug("Do attack on self casting, really wanted?");
                            targets.Add(arg.Caster);
                            break;
                    }
                }
            }
            #endregion

            #region Attack Action
            foreach (ActorExt i in targets)
            {
                SkillAffectedActor res = new SkillAffectedActor();
                SkillAffectedActor bonus = null;
                res.Result = CalcAttackResult(arg.Caster, i, arg.CastMode == SkillCastMode.Coordinate || arg.Skill.BaseData.SkillType == SkillType.NoTarget, arg.Skill.BaseData.CastRangeMax);
                res.Target = i;
                if (res.Result != SkillAttackResult.Avoid && res.Result != SkillAttackResult.Miss)
                {
                    switch (res.Result)
                    {
                        case SkillAttackResult.Counter:
                            {
                                Task task;
                                if (i.Tasks.TryGetValue("CounterEnemy", out task))
                                {
                                    SkillHandlers.KungfuMaster.Additions.CounterEnemy counter = task as SkillHandlers.KungfuMaster.Additions.CounterEnemy;
                                    counter.Deactivate();
                                    SkillArg arg2 = new SkillArg();
                                    ActorPC pc = i as ActorPC;
                                    Skill skill;
                                    if (pc != null && pc.Skills.TryGetValue(counter.CounterSkillID, out skill))
                                    {
                                        if (pc.Job == Job.KungfuMaster)
                                        {
                                            Interlocked.Add(ref pc.MP, 3);
                                            if (pc.MP > pc.MaxMP)
                                                Interlocked.Exchange(ref pc.MP, pc.MaxMP);
                                            pc.Client().SendPlayerMP();
                                        }
                                        arg2.Skill = skill;
                                        arg2.Caster = i;
                                        arg2.Target = arg.Caster;
                                        arg2.SkillSession = (byte)Global.Random.Next(0, 255);
                                        arg2.Dir = i.Dir;
                                        SkillCast(arg2);
                                    }
                                }
                                if (i.Tasks.TryGetValue("WoodBlock", out task))
                                {
                                    SkillHandlers.Assassin.Additions.WoodBlock counter = task as SkillHandlers.Assassin.Additions.WoodBlock;
                                    counter.Deactivate();
                                    SkillArg arg2 = new SkillArg();
                                    ActorPC pc = i as ActorPC;
                                    Skill skill;
                                    if (pc != null && pc.Skills.TryGetValue(counter.CounterSkillID, out skill))
                                    {
                                        arg2.Skill = skill;
                                        arg2.Caster = i;
                                        arg2.Target = arg.Caster;
                                        arg2.SkillSession = (byte)Global.Random.Next(0, 255);
                                        arg2.Dir = i.Dir;
                                        SkillCast(arg2);
                                    }
                                }
                            }
                            break;
                        case SkillAttackResult.TotalParry:
                            #region TotalParry
                            {
                                ActorPC pc = i as ActorPC;
                                if (pc != null)
                                {
                                    if (pc.Job == Job.BladeMaster && pc.MP < pc.MaxMP)
                                    {
                                        Interlocked.Increment(ref pc.MP);
                                        pc.Client().SendPlayerMP();
                                    }
                                }
                            }
#endregion
                            break;
                        default:
                            #region default attacking
                            {
                                int bonusCount, bonusDmg;
                                uint bonusAdditionID;
                                int dmg = CalcDamage(arg, i, out bonusCount, out bonusDmg, out bonusAdditionID);
                                switch (res.Result)
                                {
                                    case SkillAttackResult.Critical:
                                        dmg = (int)(dmg * 1.2f);
                                        bonusDmg = (int)(bonusDmg * 1.2f);
                                        break;
                                    case SkillAttackResult.Parry:
                                        dmg = (int)(dmg * 0.3f);
                                        bonusDmg = (int)(bonusDmg * 0.3f);
                                        break;
                                }
                                res.Damage = dmg;
                                ApplyDamage(arg.Caster, i, dmg);
                                if (bonusDmg > 0)
                                {
                                    bonus = new SkillAffectedActor();
                                    bonus.Damage = bonusDmg;
                                    bonus.Target = i;
                                    bonus.BonusAdditionID = bonusAdditionID;
                                    ApplyDamage(arg.Caster, i, bonusDmg);
                                }
                                BNSActorEventHandler handler = i.EventHandler as BNSActorEventHandler;
                                if (handler != null)
                                {
                                    handler.OnSkillDamage(arg, res.Result, dmg, bonusCount);
                                }
                                ActorPC pc = i as ActorPC;
                                if (pc != null)
                                {
                                    Network.Client.GameSession client = pc.Client();
                                    if (client != null)
                                        client.ChangeCombatStatus(true);
                                    Task task;
                                    if (pc.Tasks.TryGetValue("CombatStatusTask", out task))
                                    {
                                        task.DueTime = 30000;
                                        task.Activate();
                                    }
                                    else
                                    {
                                        Tasks.Player.CombatStatusTask ct = new Tasks.Player.CombatStatusTask(30000, pc);
                                        pc.Tasks["CombatStatusTask"] = ct;
                                        ct.Activate();
                                    }
                                }
                            }
#endregion
                            break;
                    }
                }
                affected.Add(res);
                if (bonus != null)
                    affected.Add(bonus);
            }
            #endregion
        }

        void ApplyDamage(ActorExt sActor, ActorExt dActor, int damage)
        {
            Interlocked.Add(ref dActor.HP, -damage);
            if (dActor.HP < 0)
            {
                Interlocked.Exchange(ref dActor.HP, 0);
            }
        }

        SkillAttackResult CalcAttackResult(ActorExt sActor, ActorExt dActor,bool ignoreRange, int maxRange)
        {
            if (dActor.Status.Invincible)
                return SkillAttackResult.Miss;

            if (sActor.DistanceToActor(dActor) > maxRange && !ignoreRange)
                return SkillAttackResult.Miss;
            int angDif = Math.Abs(sActor.Dir - dActor.Dir);
            if (dActor.Status.Blocking && angDif >= 90)
                return SkillAttackResult.TotalParry;
            if (dActor.Status.Counter && angDif >= 90)
                return SkillAttackResult.Counter;
            if (dActor.Status.Dummy)
                return SkillAttackResult.Counter;
            if (Global.Random.Next(0, 99) <= 95 || dActor.Status.Down)
            {
                int random = Global.Random.Next(0, 99);
                if (random <= 15)
                    return SkillAttackResult.Critical;
                else if (random <= 18 && !dActor.Status.Down)
                    return SkillAttackResult.Avoid;
                else if (random <= 20 && !dActor.Status.Down)
                    return SkillAttackResult.Parry;
                else
                    return SkillAttackResult.Normal;
            }
            else
            {
                return SkillAttackResult.Miss;
            }
        }

        int CalcDamage(SkillArg arg, ActorExt target,out int bonusCount,out int bonusDamage,out uint bonusAdditionID)
        {
            int dmgMin = (int)(arg.Caster.Status.AtkMin * arg.Skill.BaseData.MinAtk);
            int dmgMax = (int)(arg.Caster.Status.AtkMax * arg.Skill.BaseData.MaxAtk);
            int dmg = Global.Random.Next(dmgMin, dmgMax);
            float rate = arg.Skill.BaseData.ActivationTimes.Count > 0 ? 1f / arg.Skill.BaseData.ActivationTimes.Count : 1f;
            dmg = (int)(dmg * rate);
            bonusCount = 0;
            bonusDamage = 0;
            bonusAdditionID = 0;
            if (!string.IsNullOrEmpty(arg.Skill.BaseData.BonusAddition))
            {
                Task task;
                if (target.Tasks.TryGetValue(arg.Skill.BaseData.BonusAddition, out task))
                {
                    SkillHandlers.Common.Additions.BonusAddition addition = task as SkillHandlers.Common.Additions.BonusAddition;
                    if (addition != null)
                    {
                        bonusCount = addition.AccumulateCount;
                        bonusDamage = (int)(dmg * (arg.Skill.BaseData.BonusRate * bonusCount));
                        bonusAdditionID = addition.BonusAdditionID;
                    }
                    task.Deactivate();
                }
            }
            return dmg;
        }
    }
}
