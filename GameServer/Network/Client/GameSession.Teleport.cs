using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network.Map;
using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SagaBNS.Common;
using SagaBNS.Common.Account;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.CharacterServer;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;
using SagaBNS.GameServer.Packets.Client;
using SagaBNS.GameServer.Skills;
using SagaBNS.GameServer.Skills.SkillHandlers;
using SagaBNS.GameServer.Skills.SkillHandlers.Common;
using SagaBNS.GameServer.Tasks.Actor;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Network.Client
{
    public partial class GameSession : Session<GamePacketOpcode>
    {
        public void SendTeleportAdd()
        {
            ushort location = Command.Commands.Instance.TeleportPoint(this).teleportId;
            if (!chara.Locations.Contains(location))
            {
                SM_LOCATION_ADD_TELEPORT p = new SM_LOCATION_ADD_TELEPORT();
                p.LocationId = location;
                chara.Locations.Add(location);
                this.Network.SendPacket(p);
            }
        }

        public void SendTeleportLoad()
        {
            SM_LOCATION_LOAD_TELEPORT p = new SM_LOCATION_LOAD_TELEPORT();
            p.Locations = chara.Locations;
            this.Network.SendPacket(p);
        }

        public void OnGotTeleportInfo(List<ushort> locations)
        {
            foreach (ushort i in locations)
            {
                chara.Locations.Add(i);
            }
            CharacterSession.Instance.GetSkills(chara.CharID, this);
        }

        public void OnTeleportRequest(CM_TELEPORT_REQUEST p)
        {
            if (chara.Locations.Contains(p.Location))
            {
                Dictionary<Common.Item.Item, ushort> items = new Dictionary<Common.Item.Item, ushort>();
                Common.Item.Item add;
                ushort total = 0;
                foreach (KeyValuePair<ushort, ushort> i in p.ExchangeItems)
                {
                    add = this.Character.Inventory.Container[Common.Item.Containers.Inventory][i.Key];
                    if (add != null)
                        if (add.Count >= i.Value && add.ItemID == 65000)
                        {
                            items.Add(add, i.Value);
                            total += i.Value;
                        }
                        else
                            break;
                }

                if (total > 0 && total < 30)
                {
                    SM_TELEPORT_REQUEST r = new SM_TELEPORT_REQUEST();
                    r.Location = p.Location;
                    r.Time = 100;
                    this.Network.SendPacket(r);

                    Teleport teleport = new Teleport();
                    SkillArg arg = new SkillArg();
                    arg.Caster = chara;
                    arg.Dir = p.Location;
                    teleport.HandleSkillActivate(arg,items);
                }
            }
        }
    }
}