﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using SmartEngine.Core;
using SmartEngine.Network.Map;
using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SmartEngine.Network.Tasks;
using SagaBNS.Common;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Account;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Map;
using SagaBNS.GameServer.Packets.Client;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;

namespace SagaBNS.GameServer.Network.Client
{   
    public partial class GameSession : Session<GamePacketOpcode>
    {
        ActorCorpse currentCorpse;
        DateTime jumpStamp = DateTime.Now;
        public void OnActorMovement(CM_ACTOR_MOVEMENT p)
        {
            if (chara == null || chara.Status.Down)
                return;
            //Logger.ShowInfo(p.ToString());
            MoveArgument arg = new MoveArgument();
            arg.BNSMoveType = p.MoveType;
            if (p.MoveType == SagaBNS.GameServer.Map.MoveType.Jump)
            {
                if (p.Unknown82 > 0)
                {
                    if (p.XDiff == 0 && p.YDiff == 0)
                        arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.Run;
                    else
                        arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.Falling;
                }
            }
            if (p.MoveType == SagaBNS.GameServer.Map.MoveType.DashJump)
            {
                if (p.Unknown82 > 0)
                {
                    arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.Falling;
                }
            }
            arg.X = p.X2;
            arg.Y = p.Y2;
            arg.Z = p.Z2;
            int xDiff = p.XDiff;
            int yDiff = p.YDiff;
            int zDiff = p.ZDiff;
            arg.PosDiffs.Enqueue(new sbyte[] { p.XDiff, p.YDiff, p.ZDiff });
            arg.Dir = p.Dir;
            arg.Speed = p.Speed;
            int distance = chara.DistanceToPoint(p.X2, p.Y2, p.Z2);
            //Logger.ShowInfo(string.Format("Distance:{0} Diff: {1},{2},{3}", distance, p.XDiff, p.YDiff, p.ZDiff));
            if (this.account.GMLevel == 0 &&((distance > 40 && p.MoveType == SagaBNS.GameServer.Map.MoveType.Run) || (distance > 65 && p.MoveType == SagaBNS.GameServer.Map.MoveType.Dash)))
            {
                if (account.GMLevel > 0)
                    Logger.ShowDebug("Skip movement");
                arg.X = chara.X;
                arg.Y = chara.Y;
                arg.Z = chara.Z;
                arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.StepForward;
            }
            
            map.MoveActor(chara, arg, true);

            Task task;
            if (chara.Tasks.TryGetValue("SwordBlocking", out task))
            {
                Buff buf = (Buff)task;
                if ((buf.TotalLifeTime - buf.RestLifeTime) > 500)
                    task.Deactivate();
            }
            if (chara.Tasks.TryGetValue("FoodRecovery", out task))
            {
                task.Deactivate();
            }
            if (chara.Tasks.TryGetValue("Teleport", out task))
            {
                task.Deactivate();
            }
            if (chara.Party != null)
            {
                Party.PartyManager.Instance.PartyMemberPositionUpdate(chara.Party, chara);
            }
        }

        public void SaveSettings(CM_SAVE_UI_SETTING p)
        {
            chara.UISettings = p.Settings;
             
        }

        public void OnActorTurn(CM_ACTOR_TURN p)
        {
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = chara;
            evt.Target = chara;
            evt.UpdateType = UpdateTypes.Actor;
            evt.AddActorPara(PacketParameter.Dir, p.Dir);
            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);

            MoveArgument arg = new MoveArgument();
            arg.X = chara.X;
            arg.Y = chara.Y;
            arg.Z = chara.Z;
            arg.Dir = p.Dir;
            arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.Run;
            arg.Speed = 0;
            map.MoveActor(chara, arg, true);

            if (chara.Party != null)
            {
                Party.PartyManager.Instance.PartyMemberDirUpdate(chara.Party, chara);
            }
        }

        public void OnActorCorpseOpen(CM_ACTOR_CORPSE_OPEN p)
        {
            ActorCorpse corspe = map.GetActor(p.ActorID) as ActorCorpse;
            if (corspe == null && map.MapObjects.ContainsKey(p.ActorID))
            {
                corspe = map.MapObjects[p.ActorID];
            }
            if (corspe != null)
            {
                if (corspe.CurrentPickingPlayer != 0 || currentCorpse != null || (corspe.Owner != chara && corspe.Owner != null && !Party.PartyManager.Instance.PartyCanLoot(chara,corspe.Owner as ActorPC)))
                {
                    SM_ACTOR_CORPSE_OPEN_FAILED p1 = new SM_ACTOR_CORPSE_OPEN_FAILED();
                    this.Network.SendPacket(p1);
                }
                else
                {
                    currentCorpse = corspe;
                    corspe.CurrentPickingPlayer = chara.ActorID;
                    SM_ACTOR_CORPSE_ITEM_LIST p1 = new SM_ACTOR_CORPSE_ITEM_LIST();
                    p1.ActorID = corspe.ActorID;
                    p1.Gold = corspe.Gold;
                    p1.Items = corspe.AvailableItems;
                    this.Network.SendPacket(p1);

                    UpdateEvent evt = new UpdateEvent();
                    evt.Actor = chara;
                    evt.Target = chara;
                    evt.UpdateType = UpdateTypes.Actor;
                    //evt.AddActorPara(PacketParameter.PickingUp, 1);
                    map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);

                    evt = new UpdateEvent();
                    evt.Actor = chara;
                    evt.Target = corspe;
                    evt.UpdateType = UpdateTypes.CorpseInteraction;
                    evt.UserData = (byte)0;
                    map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);
                }
            }
            else
            {
                SM_ACTOR_CORPSE_OPEN_FAILED p1 = new SM_ACTOR_CORPSE_OPEN_FAILED();
                this.Network.SendPacket(p1);
            }
        }

        public void OnActorCorpseClose(CM_ACTOR_CORPSE_CLOSE p)
        {
            SendActorCorpseClose(p.ActorID);
        }

        public void OnActorItemPickUp(CM_ACTOR_ITEM_PICK_UP p)
        {
            ActorItem item = map.GetActor(p.ActorID) as ActorItem;
            if (item != null)
            {
                if (chara.DistanceToActor(item) < 50)
                {
                    Task removed;
                    if (item.Tasks.TryRemove("ItemDelete", out removed))
                        removed.Deactivate();
                    chara.HoldingItem = item;
                    SendHoldItem(item);

                    UpdateEvent evt = new UpdateEvent();
                    evt.UpdateType = UpdateTypes.ItemHide;
                    evt.Actor = chara;
                    evt.Target = item;
                    map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);

                    Tasks.Player.ActorItemPickDropTask task = new Tasks.Player.ActorItemPickDropTask(chara, item, Tasks.Player.ActorItemPickDropTask.ActionTypes.Pick);
                    chara.Tasks["ActorItemPickDropTask"] = task;
                    task.Activate();
                }
            }
        }

        public void OnActorItemDrop(CM_ACTOR_ITEM_DROP p)
        {
            if (chara.HoldingItem != null)
            {
                ActorItem item = chara.HoldingItem;
                Tasks.Actor.ItemDeleteTask deleteTask = new Tasks.Actor.ItemDeleteTask(item);
                deleteTask.Activate();
                SendHoldItemCancel(chara.HoldingItem.ObjectID, false);

                MoveArgument arg = new MoveArgument();
                arg.X = p.X;
                arg.Y = p.Y;
                arg.Z = p.Z;
                arg.BNSMoveType = SagaBNS.GameServer.Map.MoveType.Run;
                map.MoveActor(item, arg);

                Tasks.Player.ActorItemPickDropTask task = new Tasks.Player.ActorItemPickDropTask(chara, item, Tasks.Player.ActorItemPickDropTask.ActionTypes.Drop);
                chara.Tasks["ActorItemPickDropTask"] = task;
                task.Activate();

                UpdateEvent evt = new UpdateEvent();
                evt.UpdateType = UpdateTypes.ItemShow;
                evt.Actor = chara;
                evt.Target = item;
                evt.X = p.X;
                evt.Y = p.Y;
                evt.Z = p.Z;
                evt.UserData = 200;
                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);
            }
        }

        public void SendActorCorpseClose(ulong actorID)
        {
            if (currentCorpse != null && currentCorpse.ActorID == actorID)
            {
                ActorCorpse corpse = currentCorpse;
                currentCorpse = null; 
                corpse.CurrentPickingPlayer = 0;
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = chara;
                evt.Target = chara;
                evt.UpdateType = UpdateTypes.Actor;
                //evt.AddActorPara(PacketParameter.PickingUp, 0);
                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);

                evt = new UpdateEvent();
                evt.Actor = chara;
                evt.Target = corpse;
                evt.UpdateType = UpdateTypes.CorpseInteraction;
                evt.UserData = (byte)1;
                map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);
                
                if (corpse != null && corpse.AvailableItems.Count == 0 && corpse.Gold == 0)
                {
                    Task task;
                    if (corpse.ActorType == ActorType.MAPOBJECT)
                    {
                        evt = new UpdateEvent();
                        evt.Actor = corpse;
                        evt.UpdateType = UpdateTypes.MapObjectVisibilityChange;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);
                    }
                    else
                    {
                        if (corpse.NPC.BaseData.CorpseItemID == 0 && corpse.NPC.BaseData.QuestIDs.Count == 0)
                        {
                            if (corpse.Tasks.TryRemove("CorpseDelete", out task))
                                task.Deactivate();
                            map.DeleteActor(corpse);
                        }
                        else
                        {
                            evt = new UpdateEvent();
                            evt.Actor = corpse;
                            evt.UpdateType = UpdateTypes.DeleteCorpse;
                            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, chara, true);
                        }
                    }
                }
            }
            SM_ACTOR_CORPSE_CLOSE_RESULT p1 = new SM_ACTOR_CORPSE_CLOSE_RESULT();
            p1.ActorID = actorID;
            this.Network.SendPacket(p1);
        }

        public void OnActorCorpsePickUp(CM_ACTOR_CORPSE_PICK_UP p)
        {
            ActorCorpse corpse = map.GetActor(p.ActorID) as ActorCorpse;
            if (corpse != null)
            {
                if (!corpse.PickUp && corpse.NPC.BaseData.CorpseItemID > 0)
                {
                    corpse.PickUp = true;
                    corpse.ShouldDisappear = true;

                    SendHoldItem(corpse.NPC.BaseData.CorpseItemID, corpse.ActorID);

                    UpdateEvent evt = new UpdateEvent();
                    evt.Actor = corpse;
                    evt.Target = chara;
                    evt.UpdateType = UpdateTypes.DeleteCorpse;
                    map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, corpse, false);

                    Tasks.Player.ActorItemPickDropTask task = new Tasks.Player.ActorItemPickDropTask(chara, corpse, Tasks.Player.ActorItemPickDropTask.ActionTypes.PickCorpse);
                    chara.Tasks["ActorItemPickDropTask"] = task;
                    task.Activate();
                }
            }
        }

        public void OnActorCorpseLoot(CM_ACTOR_CORPSE_LOOT p)
        {
            if (currentCorpse != null && currentCorpse.ActorID == p.ActorID)
            {
                foreach (byte i in p.Indices)
                {
                    if (i == 255)
                    {
                        Interlocked.Add(ref chara.Gold, currentCorpse.Gold);
                        currentCorpse.Gold = 0;
                        SendPlayerGold();
                    }
                    else if (currentCorpse.Items.Length > i)
                    {
                        if (chara.Inventory.FindFreeIndex(Common.Item.Containers.Inventory) >= 0)
                        {
                            if (currentCorpse.Items[i] != null)
                            {
                                AddItem(currentCorpse.Items[i]);
                                currentCorpse.Items[i] = null;
                            }
                        }
                        else
                        { 
                        }
                    }
                }
                SM_ACTOR_CORPSE_LOOT_RESULT p2 = new SM_ACTOR_CORPSE_LOOT_RESULT();
                p2.ActorID = p.ActorID;
                p2.Indices = p.Indices;
                this.Network.SendPacket(p2);
            }
        }

        public void SendActorUpdates(List<UpdateEvent> events)
        {
            SM_ACTOR_UPDATE_LIST p = new SM_ACTOR_UPDATE_LIST();
            p.Events = events;
            this.Network.SendPacket(p);
            /*foreach (UpdateEvent i in events)
            {
                i.Actor = null;
                i.Target = null;
                i.UserData = null;
                i.UserData2 = null;                
            }*/
            events.Clear();
        }

        public void SendActorAppear(List<Actor> appearActors, List<Actor> disappearActors)
        {
            SM_ACTOR_APPEAR_LIST p = new SM_ACTOR_APPEAR_LIST();
            p.MapInstanceID = map.InstanceID;
            p.DisappearActors = disappearActors;
            p.AppearActors = appearActors;

            this.Network.SendPacket(p);

            SM_ACTOR_APPEAR_INFO_LIST p2 = new SM_ACTOR_APPEAR_INFO_LIST();
            p2.PlayerFaction = chara.Faction;
            p2.MapInstanceID = map.InstanceID;
            p2.DisappearActors = disappearActors;
            p2.AppearActors = appearActors;
            this.Network.SendPacket(p2);
        }

        public void SendActorAggro(ulong actorID)
        {
            SM_ACTOR_AGGRO p = new SM_ACTOR_AGGRO();
            p.ActorID = actorID;
            this.Network.SendPacket(p);
        }
    }
}
