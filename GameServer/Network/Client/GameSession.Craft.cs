using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network.Map;
using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SagaBNS.Common;
using SagaBNS.Common.Account;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.CharacterServer;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;
using SagaBNS.GameServer.Packets.Client;
using SagaBNS.GameServer.Tasks.Actor;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Network.Client
{
    public partial class GameSession : Session<GamePacketOpcode>
    {
        public void OnAddCraft(CM_ADD_CRAFT p)
        {
            SM_ADD_CRAFT r = new SM_ADD_CRAFT();
            SM_PLAYER_UPDATE_LIST r2 = new SM_PLAYER_UPDATE_LIST();
            this.Network.SendPacket(r);
            UpdateEvent Event = new UpdateEvent();
            switch ((Crafts)p.Craft)
            {
                case Crafts.Logger:
                case Crafts.Horticulture:
                case Crafts.Harvester:
                case Crafts.Herbalist:
                case Crafts.Miner:
                case Crafts.StoneCutter:
                case Crafts.Fisher:
                    {
                        if (this.Character.Craft1 == Crafts.None)
                        {
                            this.Character.Craft1 = (Crafts)p.Craft;
                            Event.AddActorPara(PacketParameter.Craft1, p.Craft);
                            r2.Parameters = Event;
                            this.Network.SendPacket(r2);
                        }
                        else if (this.Character.Craft2 == Crafts.None)
                        {
                            this.Character.Craft2 = (Crafts)p.Craft;
                            Event.AddActorPara(PacketParameter.Craft2, p.Craft);
                            r2.Parameters = Event;
                            this.Network.SendPacket(r2);
                        }
                    }
                    break;

                case Crafts.Cooking:
                case Crafts.Potion:
                case Crafts.WeaponSmith:
                case Crafts.ForceSmith:
                case Crafts.BoPae:
                case Crafts.Jewel:
                case Crafts.Pottery:
                    {
                        if (this.Character.Craft3 == Crafts.None)
                        {
                            this.Character.Craft3 = (Crafts)p.Craft;
                            Event.AddActorPara(PacketParameter.Craft3, p.Craft);
                            r2.Parameters = Event;
                            this.Network.SendPacket(r2);
                        }
                        else if (this.Character.Craft4 == Crafts.None)
                        {
                            this.Character.Craft4 = (Crafts)p.Craft;
                            Event.AddActorPara(PacketParameter.Craft4, p.Craft);
                            r2.Parameters = Event;
                            this.Network.SendPacket(r2);
                        }
                    }
                    break;

                default:
                    {
                        SM_SERVER_MESSAGE r3 = new SM_SERVER_MESSAGE();
                        r3.MessagePosition = SM_SERVER_MESSAGE.Positions.ChatWindow;
                        r3.Message = "Unknown Craft please report to LokiReborn.";
                        this.Network.SendPacket(r3);
                    }
                    break;
            }
        }
      
        public void OnDeleteCraft(CM_DELETE_CRAFT p)
        {
            SM_PLAYER_UPDATE_LIST r = new SM_PLAYER_UPDATE_LIST();
            UpdateEvent Event = new UpdateEvent();
            if (this.Character.Craft1 == (Crafts)p.Craft)
            {
                this.Character.Craft1 = Crafts.None;
                Event.AddActorPara(PacketParameter.Craft1, 0);
                r.Parameters = Event;
                this.Network.SendPacket(r);
            }
            else if (this.Character.Craft2 == (Crafts)p.Craft)
            {
                this.Character.Craft2 = Crafts.None;
                Event.AddActorPara(PacketParameter.Craft2, 0);
                r.Parameters = Event;
                this.Network.SendPacket(r);
            }
            else if (this.Character.Craft3 == (Crafts)p.Craft)
            {
                this.Character.Craft3 = Crafts.None;
                Event.AddActorPara(PacketParameter.Craft3, 0);
                r.Parameters = Event;
                this.Network.SendPacket(r);
            }
            else if (this.Character.Craft4 == (Crafts)p.Craft)
            {
                this.Character.Craft4 = Crafts.None;
                Event.AddActorPara(PacketParameter.Craft4, 0);
                r.Parameters = Event;
                this.Network.SendPacket(r);
            }
        }
    }
}
