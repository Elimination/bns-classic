﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Utils;
using SagaBNS.Common;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Account;
using SagaBNS.GameServer.Map;
using SagaBNS.GameServer.Packets.Client;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;
using SagaBNS.GameServer.Services;

namespace SagaBNS.GameServer.Network.Client
{   
    public partial class GameSession : Session<GamePacketOpcode>
    {
        static ulong nextSessionID = 0x1000000000001;
        Account account;
        string username;
        Guid accountGUID, slotGUID, tokenGUID;
        ActorPC chara;
        DateTime loginTime;
        Map.Map map;
        BroadcastService broadcastService;

        public bool Authenticated { get; set; }
        public ulong SessionID { get; set; }

        public BroadcastService BroadcastService { get { return broadcastService; } }

        public Account Account { get { return account; } }

        public Map.Map Map { get { return map; } }

        public ActorPC Character { get { return chara; } }

        public void CharacterSelect()
        {
            SM_CHARACTER_SELECT p1 = new SM_CHARACTER_SELECT();
            p1.Token = Conversions.HexStr2Bytes("0000000000000000000000000000");
            this.Network.SendPacket(p1);
  
        }

        public void OnLoginAuth(Packets.Client.CM_LOGIN_AUTH p)
        {
            CharacterSession.Instance.RequestCharInfo((uint)p.CharID, this); 
            //Logger.ShowInfo(username + " is trying to login");
        }

        public void OnActorInfo(ActorPC pc)
        {
            if (pc != null)
            {
                this.chara = pc;
                AccountSession.Instance.RequestAccountInfo(pc.AccountID, this);
                return;
            }
            Logger.ShowInfo("Login result: No such character");
            Network.Disconnect();

        }

        void SendAuthFinish()
        {
            SM_LOGIN_AUTH_RESULT p = new SM_LOGIN_AUTH_RESULT();
            this.Network.SendPacket(p);
        }

        public void OnAccountInfo(Common.Packets.AccountServer.AccountLoginResult result, Account acc)
        {
            if (acc != null && acc.UserName != null && acc.UserName != "")
            {
                Logger.ShowInfo(string.Format("Login result for player {0}:{1}", username, result));
                if (result == Common.Packets.AccountServer.AccountLoginResult.OK)
                {
                    foreach (GameSession i in Manager.GameClientManager.Instance.Clients.ToArray())
                    {
                        if (i.account != null && i.account.AccountID == acc.AccountID)
                        {
                            i.Network.Disconnect();
                            AccountSession.Instance.RequestAccountInfo(username, this);
                            return;
                        }
                    }
                    this.account = acc;
                    OnGotChar(chara);
                    return;
                }
                else
                {
                    Logger.ShowInfo("Login result for " + username + ": No such account");
                    Network.Disconnect();
                }
            }
            Logger.ShowInfo("Login result: No such account");
            Network.Disconnect();
        }

        public void OnGotChar(ActorPC pc)
        {
            Logger.ShowInfo(string.Format("Finishing login for player {0}", username));
            if (pc.Offline && pc.PartyID > 0)
            {
                Common.Party.Party party;
                if (Party.PartyManager.Instance.Parties.TryGetValue(pc.PartyID, out party))
                {
                    this.chara = Party.PartyManager.Instance.PartyGetMember(party, pc.CharID);
                    if (chara != null)
                    {
                        SmartEngine.Network.Tasks.Task task;
                        if (chara.Tasks.TryGetValue("PartyOfflineTask", out task))
                            task.Deactivate();
                        SendAuthFinish();
                        return;
                    }
                }
            }
            this.chara = pc;
            Logger.ShowInfo("Login result for " + username + ": OK");
            Logger.ShowInfo("Player " + account.UserName + " selected character:" + pc.Name);
            CharacterSession.Instance.GetInventory(chara.CharID, this);

            return;
        }

        public void OnLoginStart(CM_LOGIN_START p)
        {
            loginTime = DateTime.Now;

            map = MapManager.Instance.GetMap(chara.MapID, chara.CharID, chara.PartyID);
            if (chara.Party != null && chara.Offline)
                this.SessionID = chara.ActorID;
            else
                this.SessionID = nextSessionID++;
            chara.EventHandler = new ActorEventHandlers.PCEventHandler(this);

            broadcastService = new Services.BroadcastService(this);
            broadcastService.Activate();
            map.RegisterActor(chara, SessionID);
        }

        public void OnLoginChatAuth(CM_LOGIN_CHAT_AUTH p)
        {
            SM_LOGIN_CHAT_AUTH p1 = new SM_LOGIN_CHAT_AUTH();
            p1.Server = (ulong)chara.WorldID;
            p1.Token = Conversions.HexStr2Bytes("4165E727FDE5F82DDA7BB8977CF8A3FBBD5C8A61EF90ED81EDAA98B6ED180740131ABD2EDAEBAB6EDE7B6B2579F3BCF3CDB257E17165D38171FD7632C1F7CC31");
            this.Network.SendPacket(p1);
        }

        public void OnAuctionHandshake(CM_LOGIN_AUCTION_START p)
        {
            if (Configuration.Instance.AuctionEnabled)
            {
                SM_LOGIN_AUCTION_START r = new SM_LOGIN_AUCTION_START();
                r.IP = Configuration.Instance.AuctionServer;
                r.Port = 11010;
                r.Token = Conversions.HexStr2Bytes("6D2BB06122A6ACADC0A80167");
                this.Network.SendPacket(r);
            }
        }

        public void OnLoginUnknown1(CM_LOGIN_UNKNOWN1 p)
        {
            SM_LOGIN_UNKNOWN1 p1 = new SM_LOGIN_UNKNOWN1();
            p1.Unknown1 = 0x1ED16;
            p1.Unknown2 = 0x4A2C;
            p1.Unknown3 = 0x4056;
            this.Network.SendPacket(p1);
        }

        public void OnLoginUnknown2(CM_LOGIN_UNKNOWN2 p)
        {
            SM_LOGIN_UNKNOWN2 p1 = new SM_LOGIN_UNKNOWN2();
            p1.Unknown1 = 0x2C4;
            this.Network.SendPacket(p1);
        }

        public void SendLoginInit()
        {
            Authenticated = true;
            SM_LOGIN_INIT p1 = new SM_LOGIN_INIT();
            PC.Status.CalcStatus(chara,false);
            p1.Player = chara;
            this.Network.SendPacket(p1);
            SendQuestList();
            SendChangeMap();
        }
    }
}
