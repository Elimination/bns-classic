using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network.Map;
using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SagaBNS.Common;
using SagaBNS.Common.Account;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.CharacterServer;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Network.AccountServer;
using SagaBNS.GameServer.Network.CharacterServer;
using SagaBNS.GameServer.Packets.Client;
using SagaBNS.GameServer.Tasks.Actor;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Network.Client
{
    public partial class GameSession : Session<GamePacketOpcode>
    {
        public void OnTradeRequest(CM_TRADE_INIT p)
        { 
        }
    }
}