﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.GameServer.Network.Client
{
    public enum ChatType
    {
        Whisper,
        General,
        UnknownParty = 0x3,
    }
}
