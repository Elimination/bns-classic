﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_COMPOSE_BAGUA_FAIL : Packet<GamePacketOpcode>
    {
        public SM_COMPOSE_BAGUA_FAIL()
        {
            this.ID = GamePacketOpcode.SM_COMPOSE_BAGUA_FAIL;
        }

        public ushort Code
        {
            set
            {
                PutUShort(value, 2);
            }
        }
    }
}
