﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_BAGUA_SET_CHANGE : Packet<GamePacketOpcode>
    {
        public SM_BAGUA_SET_CHANGE()
        {
            this.ID = GamePacketOpcode.SM_BAGUA_SET_CHANGE;
        }
    }
}
