﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Utils;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Item;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Map;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_ACTOR_CORPSE_OPEN_FAILED : Packet<GamePacketOpcode>
    {
        public SM_ACTOR_CORPSE_OPEN_FAILED()
        {
            this.ID = GamePacketOpcode.SM_ACTOR_CORPSE_OPEN_FAILED;
            PutUShort(0x11D, 2);
        }
    }
}
