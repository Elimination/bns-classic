﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Utils;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Map;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_PLAYER_UPDATE_LIST : Packet<GamePacketOpcode>
    {
        public SM_PLAYER_UPDATE_LIST()
        {
            this.ID = GamePacketOpcode.SM_PLAYER_UPDATE_LIST;
        }

        public UpdateEvent Parameters
        {
            set
            {
                PutUShort(1, 7);//Set Count
                PutUShort((ushort)value.ActorUpdateParameters.Count);//Count
                ushort lenOff = offset;
                PutUShort(0);
                foreach (ActorUpdateParameter j in value.ActorUpdateParameters)
                {
                    PutShort((short)j.Parameter);
                    j.Write(this);
                }
                PutUShort((ushort)(Length - lenOff - 2), lenOff);
                PutInt((int)Length - 6, 2);
            }        
        }
    }
}
