﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Utils;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Item;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.GameServer;
using SagaBNS.GameServer.Map;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_ACTOR_CORPSE_LOOT_RESULT : Packet<GamePacketOpcode>
    {
        public SM_ACTOR_CORPSE_LOOT_RESULT()
        {
            this.ID = GamePacketOpcode.SM_ACTOR_CORPSE_LOOT_RESULT;
        }

        public ulong ActorID
        {
            set
            {
                PutULong(value, 2);
            }
        }

        public byte[] Indices
        {
            set
            {
                PutShort((short)value.Length, 10);
                PutBytes(value);
                foreach (byte i in value)
                    PutByte(i == 255 ? (byte)0 : (byte)1);
            }
        }
    }
}
