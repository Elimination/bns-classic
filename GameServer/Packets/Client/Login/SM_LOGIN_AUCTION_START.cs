﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_LOGIN_AUCTION_START : Packet<GamePacketOpcode>
    {
        public SM_LOGIN_AUCTION_START()
        {
            this.ID = GamePacketOpcode.SM_LOGIN_AUCTION_START;
        }

        public byte[] IP
        {
            set
            {
                PutBytes(value,2);
            }
        }

        public ushort Port
        {
            set
            {
                PutUShort(value,6);
            }
        }

        public byte[] Token
        {
            set
            {
                PutBytes(value,8);
            }
        }
    }
}
