﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_LOGIN_UNKNOWN2 : Packet<GamePacketOpcode>
    {
        public SM_LOGIN_UNKNOWN2()
        {
            this.ID = GamePacketOpcode.SM_LOGIN_UNKNOWN2;
        }

        public short Unknown1
        {
            set
            {
                PutShort(value, 2);
            }
        }
    }
}
