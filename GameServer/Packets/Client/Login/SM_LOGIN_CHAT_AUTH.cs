﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_LOGIN_CHAT_AUTH : Packet<GamePacketOpcode>
    {
        public SM_LOGIN_CHAT_AUTH()
        {
            this.ID = GamePacketOpcode.SM_LOGIN_CHAT_AUTH;
        }

        public ulong Server
        {
            set
            {
                PutULong(value,2);
            }
        }

        public byte[] Token
        {
            set
            {
                PutUShort((ushort)value.Length, 10);
                PutBytes(value);
            }
        }
    }
}
