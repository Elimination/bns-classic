﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_CHARACTER_SELECT : Packet<GamePacketOpcode>
    {
        public SM_CHARACTER_SELECT()
        {
            this.ID = GamePacketOpcode.SM_CHARACTER_SELECT;
        }

        public byte[] Token
        {
            set
            {
                PutBytes(value);
            }
        }
    }
}
