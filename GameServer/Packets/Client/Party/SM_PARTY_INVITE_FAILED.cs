﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_PARTY_INVITE_FAILED : Packet<GamePacketOpcode>
    {
        public enum Reasons
        {
            ALREADY_SENT,
        }
        public SM_PARTY_INVITE_FAILED()
        {
            this.ID = GamePacketOpcode.SM_PARTY_INVITE_FAILED;
        }

        public Reasons Reason
        {
            set
            {
                PutInt((int)value, 2);
            }
        }
    }
}
