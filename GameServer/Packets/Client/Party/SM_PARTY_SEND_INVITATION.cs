﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_PARTY_SEND_INVITATION : Packet<GamePacketOpcode>
    {
        public SM_PARTY_SEND_INVITATION()
        {
            this.ID = GamePacketOpcode.SM_PARTY_SEND_INVITATION;
        }

        public ulong PartyID
        {
            set
            {
                PutULong(value, 2);
            }
        }

        public ulong ActorID
        {
            set
            {
                PutULong(value, 10);
            }
        }

        public string Name
        {
            set
            {
                PutUShort((ushort)(value.Length), 18);
                PutBytes(Encoding.Unicode.GetBytes(value));
                PutUInt(0);
            }
        }
    }
}
