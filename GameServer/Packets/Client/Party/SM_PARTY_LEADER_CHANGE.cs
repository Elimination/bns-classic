﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_PARTY_LEADER_CHANGE : Packet<GamePacketOpcode>
    {
        public SM_PARTY_LEADER_CHANGE()
        {
            this.ID = GamePacketOpcode.SM_PARTY_LEADER_CHANGE;
        }

        public ulong ActorID
        {
            set
            {
                PutULong(value, 2);
            }
        }
    }
}
