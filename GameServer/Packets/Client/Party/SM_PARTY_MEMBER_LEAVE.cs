﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_PARTY_MEMBER_LEAVE : Packet<GamePacketOpcode>
    {
        public SM_PARTY_MEMBER_LEAVE()
        {
            this.ID = GamePacketOpcode.SM_PARTY_MEMBER_LEAVE;
        }

        public string MemberName
        {
            set
            {
                PutShort((short)value.Length, 2);
                PutBytes(Encoding.Unicode.GetBytes(value));
                PutByte(0);
            }
        }
    }
}
