﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_MAP_PORTAL_SOME_REPLY : Packet<GamePacketOpcode>
    {
        public SM_MAP_PORTAL_SOME_REPLY()
        {
            this.ID = GamePacketOpcode.SM_MAP_PORTAL_SOME_REPLY;

            PutUShort(0x202);
        }
    }
}
