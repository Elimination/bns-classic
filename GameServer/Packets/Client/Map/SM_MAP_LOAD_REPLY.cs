﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_MAP_LOAD_REPLY : Packet<GamePacketOpcode>
    {
        public SM_MAP_LOAD_REPLY()
        {
            this.ID = GamePacketOpcode.SM_MAP_LOAD_REPLY;

            PutInt(0);
            PutInt(0);
        }
    }
}
