﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_MAP_PORTAL_ACTIVATE : Packet<GamePacketOpcode>
    {
        public SM_MAP_PORTAL_ACTIVATE()
        {
            this.ID = GamePacketOpcode.SM_MAP_PORTAL_ACTIVATE;

            PutInt(0x0201);
            PutShort(0);
            PutInt(0);
            PutInt(0);
            PutInt(0);
            PutInt(0);
            PutInt(0);
        }

        public byte U1
        {
            set
            {
                PutByte(value, 3);
            }
        }

        public byte U3
        {
            set
            {
                PutByte(value, 2);
            }
        }

        public int DisappearEffect
        {
            set
            {
                PutInt(value, 4);
            }
        }

        public int U2
        {
            set
            {
                PutInt(value, 20);
            }
        }

        public uint CutScene
        {
            set
            {
                if (value > 0)
                {
                    PutUInt(value, 12);
                }
            }
        }
    }
}
