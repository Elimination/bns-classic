﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_TELEPORT_REQUEST : Packet<GamePacketOpcode>
    {
        public SM_TELEPORT_REQUEST()
        {
            this.ID = GamePacketOpcode.SM_TELEPORT_REQUEST;
        }

        public ushort Location
        {
            set
            {
                PutUShort(value, 2);
            }
        }

        public ushort Time
        {
            set
            {
                PutUShort(value, 4);
            }
        }
    }
}
