using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Quests;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_LOCATION_LOAD_TELEPORT : Packet<GamePacketOpcode>
    {
        public SM_LOCATION_LOAD_TELEPORT()
        {
            this.ID = GamePacketOpcode.SM_LOCATION_LOAD_TELEPORT;
        }

        public List<ushort> Locations
        {
            set
            {
                BigInteger val = new BigInteger();
                foreach (ushort i in value)
                {
                    val |= (BigInteger.One << (i - 1));
                }
                byte[] buf = val.ToByteArray();
                if (value.Count() == 0)
                {
                    PutUShort(0);

                }
                else
                {
                    PutUShort((ushort)buf.Length, 2);
                    PutBytes(buf);
                }
            }
        }
    }
}
