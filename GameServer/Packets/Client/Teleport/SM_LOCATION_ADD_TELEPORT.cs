using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_LOCATION_ADD_TELEPORT : Packet<GamePacketOpcode>
    {
        public SM_LOCATION_ADD_TELEPORT()
        {
            this.ID = GamePacketOpcode.SM_LOCATION_ADD_TELEPORT;
        }

        public ushort LocationId
        {
            set
            {
                PutUShort(value, 2);
            }
        }
    }
}
