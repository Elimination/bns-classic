﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_EXPAND_INVENTORY : Packet<GamePacketOpcode>
    {
        public SM_EXPAND_INVENTORY()
        {
            this.ID = GamePacketOpcode.SM_EXPAND_INVENTORY;
        }
    }
}
