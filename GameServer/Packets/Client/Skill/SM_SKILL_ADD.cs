﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_SKILL_ADD : Packet<GamePacketOpcode>
    {
        public SM_SKILL_ADD()
        {
            this.ID = GamePacketOpcode.SM_SKILL_ADD;
        }

        public uint SkillID
        {
            set
            {
                PutUInt(value, 2);
                PutByte(0);
            }
        }
    }
}
