using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Skills;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_SKILL_LOAD : Packet<GamePacketOpcode>
    {
        public SM_SKILL_LOAD()
        {
            this.ID = GamePacketOpcode.SM_SKILL_LOAD;
        }

        public List<Skill> Skills
        {
            set
            {
                PutUShort((ushort)value.Count);
                foreach (Skill i in value)
                {
                    PutUInt(i.ID);
                    PutByte(0);
                }
            }
        }
    }
}
