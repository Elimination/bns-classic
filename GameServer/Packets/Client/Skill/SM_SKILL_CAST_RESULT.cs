﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_SKILL_CAST_RESULT : Packet<GamePacketOpcode>
    {
        public SM_SKILL_CAST_RESULT()
        {
            this.ID = GamePacketOpcode.SM_SKILL_CAST_RESULT;
        }

        public byte SkillSession
        {
            set
            {
                PutByte(value, 2);
            }
        }

        public ushort Unknown
        {
            set
            {
                PutUShort(value, 3);
            }
        }

        public uint SkillID
        {
            set
            {
                PutUInt(value, 5);
                PutByte(0);
            }
        }
    }
}
