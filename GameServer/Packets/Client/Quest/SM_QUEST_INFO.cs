﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Quests;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_INFO : Packet<GamePacketOpcode>
    {
        public SM_QUEST_INFO()
        {
            this.ID = GamePacketOpcode.SM_QUEST_INFO;
        }

        public List<Quest> Quests
        {
            set
            {
                PutShort((short)value.Count, 2);
                foreach (Quest i in value)
                {
                    PutUShort(i.QuestID);
                    PutByte(i.NextStep);
                    PutShort(i.Flag1);
                    PutShort(i.Flag2);
                    PutShort(i.Flag3);
                    PutShort(0);
                    PutShort(0);
                    PutShort(0);
                }
            }
        }
    }
}
