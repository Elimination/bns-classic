﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Quests;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_HISTORY : Packet<GamePacketOpcode>
    {
        public SM_QUEST_HISTORY()
        {
            this.ID = GamePacketOpcode.SM_QUEST_HISTORY;
        }

        public List<ushort> QuestsCompelted
        {
            set
            {
                BigInteger val = new BigInteger();
                foreach (ushort i in value)
                {
                    val |= (BigInteger.One << (i - 1));
                }
                byte[] buf = val.ToByteArray();
                PutUShort((ushort)buf.Length, 2);
                PutBytes(buf);
                PutUShort(0);
                PutUShort(0);
            }
        }
    }
}
