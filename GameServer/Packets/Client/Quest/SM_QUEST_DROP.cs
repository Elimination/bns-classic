﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_DROP : Packet<GamePacketOpcode>
    {
        public SM_QUEST_DROP()
        {
            this.ID = GamePacketOpcode.SM_QUEST_DROP;
        }

        public ushort Quest
        {
            set
            {
                PutUShort(value,2);
            }
        }
    }
}
