﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_FINISH : Packet<GamePacketOpcode>
    {
        public SM_QUEST_FINISH()
        {
            this.ID = GamePacketOpcode.SM_QUEST_FINISH;
        }

        public ushort QuestID
        {
            set
            {
                PutUShort(value, 2);
            }
        }

        public byte Step
        {
            set
            {
                PutByte(value, 4);
            }
        }

        public byte StepStatus
        {
            set
            {
                PutByte(value, 5);
            }
        }
    }
}
