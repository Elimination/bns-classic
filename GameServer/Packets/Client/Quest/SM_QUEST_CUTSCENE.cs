﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_CUTSCENE : Packet<GamePacketOpcode>
    {
        public SM_QUEST_CUTSCENE()
        {
            this.ID = GamePacketOpcode.SM_QUEST_CUTSCENE;
        }

        public uint CutsceneID
        {
            set
            {
                PutUInt(value, 2);
            }
        }
    }
}
