﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_QUEST_NPC_OPEN : Packet<GamePacketOpcode>
    {
        public SM_QUEST_NPC_OPEN()
        {
            this.ID = GamePacketOpcode.SM_QUEST_NPC_OPEN;
        }

        public ulong ActorID
        {
            set
            {
                PutULong(value, 2);
            }
        }
    }
}
