﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SagaBNS.Common.Item;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Item;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_ITEM_INFO : Packet<GamePacketOpcode>
    {
        ItemUpdateMethod curMethod;
        public SM_ITEM_INFO()
        {
            this.ID = GamePacketOpcode.SM_ITEM_INFO;
        }

        public ItemUpdateMethod Reason
        {
            set
            {
                PutByte((byte)value, 6);
                curMethod = value;
            }
        }

        public List<Common.Item.Item> Items
        {
            set
            {
                PutUShort((ushort)value.Count, 7);//Count
                foreach (Common.Item.Item i in value)
                {
                    i.ToPacket(this);
                }

                PutInt((int)Length - 6, 2);
            }
        }
    }
}
