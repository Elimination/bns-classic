﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_CHAT_RESPONSE : Packet<GamePacketOpcode>
    {
        public SM_CHAT_RESPONSE()
        {
            this.ID = GamePacketOpcode.SM_CHAT_RESPONSE;
        }

        public ushort MessageId
        {
            set 
            {
                PutUShort(value, 2);
            }
        }
    }
}
