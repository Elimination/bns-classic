﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Packets.Client
{
    public class SM_ADD_CRAFT : Packet<GamePacketOpcode>
    {
        public SM_ADD_CRAFT()
        {
            this.ID = GamePacketOpcode.SM_ADD_CRAFT;
        }
    }
}
