﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network.Tasks;

namespace SagaBNS.GameServer.Map
{
    public class MapInstanceDestoryTask : Task
    {
        Map map;
        public MapInstanceDestoryTask(Map map)
            : base(600000, 60000, "MapInstanceDestory")
        {
            this.map = map;
        }

        public override void CallBack()
        {
            this.Deactivate();
            MapManager.Instance.DeleteMapInstance(map);
            this.map = null;
        }
    }
}
