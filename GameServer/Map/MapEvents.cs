﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.GameServer.Map
{
    public enum MapEvents
    {
        APPEAR,
        DISAPPEAR,
        EVENT_BROADCAST,
        CHAT,
        PORTAL_ENTER,
        QUEST_UPDATE,
    }
}
