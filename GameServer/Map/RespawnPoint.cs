﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SagaBNS.GameServer.Map
{
    public struct RespawnPoint
    {
        public uint MapID;
        public short X, Y, Z;
        public ushort Dir, teleportId;
    }
}
