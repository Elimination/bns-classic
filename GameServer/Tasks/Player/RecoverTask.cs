﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Player
{
    public class RecoverTask : Buff
    {
        GameSession client;
        public RecoverTask(GameSession client)
            : base(client.Character, "RecoverTask", 40000)
        {
            this.client = client;
            this.OnAdditionStart += new StartEventHandler(DieTask_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(DieTask_OnAdditionEnd);

            Task removed;
            if (client.Character.Tasks.TryRemove("RecoverTask", out removed))
                removed.Deactivate();
        }

        void DieTask_OnAdditionEnd(SmartEngine.Network.Map.Actor actor, Buff skill, bool cancel)
        {
            Task removed;
            client.Character.Tasks.TryRemove("RecoverTask", out removed);
            client.Character.Status.Recovering = false;
            if (!cancel)
            {
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = client.Character;
                evt.Target = client.Character;
                evt.UpdateType = UpdateTypes.Actor;
                //evt.AddActorPara(Common.Packets.GameServer.PacketParameter.Dead, 0);
                client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
                Interlocked.Exchange(ref client.Character.HP, (int)(client.Character.MaxHP * 0.7f));
                client.SendPlayerHP();
                client.Character.Status.Dead = false;
                client.Character.Status.Dying = false;

                evt = new UpdateEvent();
                evt.Actor = client.Character;
                evt.Target = client.Character;
                evt.UpdateType = UpdateTypes.PlayerRecover;
                evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Activate;
                client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
            }
            else
            {
                UpdateEvent evt = new UpdateEvent();
                evt.Actor = client.Character;
                evt.Target = client.Character;
                evt.UpdateType = UpdateTypes.PlayerRecover;
                evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
                client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
            }
        }

        void DieTask_OnAdditionStart(SmartEngine.Network.Map.Actor actor, Buff skill)
        {
            Task removed;
            if (client.Character.Tasks.TryRemove("DieTask", out removed))
                removed.Deactivate();
            client.Character.Status.Recovering = true;
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = client.Character;
            evt.Target = client.Character;
            evt.UpdateType = UpdateTypes.PlayerRecover;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.None;
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);

            evt = new UpdateEvent();
            evt.Actor = client.Character;
            evt.Target = client.Character;
            evt.AdditionID = 65000;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(Common.Packets.GameServer.PacketParameter.Dead, 3);
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
        }
    }
}
