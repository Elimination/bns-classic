﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Network.Client;
namespace SagaBNS.GameServer.Tasks.Player
{
    public class PartyOfflineTask : Task
    {
        ActorPC pc;
        public PartyOfflineTask(ActorPC pc)
            : base(300000, 300000, "PartyOfflineTask")
        {
            this.pc = pc;
            pc.Tasks["PartyOfflineTask"] = this;
        }

        protected override void OnDeactivate()
        {
            Task removed;
            pc.Tasks.TryRemove("PartyOfflineTask", out removed);            
        }

        public override void CallBack()
        {
            this.Deactivate();
            if (pc.Party != null)
            {
                Party.PartyManager.Instance.PartyMemberQuit(pc.Party, pc);
            }
        }
    }
}
