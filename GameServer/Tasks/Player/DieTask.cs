﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Player
{
    public class DieTask : Buff
    {
        GameSession client;
        public DieTask(GameSession client)
            : base(client.Character, "DieTask", 80000)
        {
            this.client = client;
            this.OnAdditionStart += new StartEventHandler(DieTask_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(DieTask_OnAdditionEnd);

            Task removed;
            if (client.Character.Tasks.TryRemove("DieTask", out removed))
                removed.Deactivate();
        }

        void DieTask_OnAdditionEnd(SmartEngine.Network.Map.Actor actor, Buff skill, bool cancel)
        {
            Task removed;
            client.Character.Tasks.TryRemove("DieTask", out removed);
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = client.Character;
            evt.Target = client.Character;
            evt.AdditionSession = 28674;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.SkillSession = 255;
            evt.AdditionID = 65010;
            evt.UpdateType = UpdateTypes.Actor;
            //evt.AddActorPara(Common.Packets.GameServer.PacketParameter.Speed, 62);
            evt.UserData = new byte[] { 9, 3, 0 };
            if (!cancel)
            {
                client.Character.Status.Dying = false;
                //evt.AddActorPara(Common.Packets.GameServer.PacketParameter.Dead, 1);
            }
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);

            evt = new UpdateEvent();
            evt.Actor = client.Character;
            evt.Target = client.Character;
            evt.AdditionSession = 28674;
            evt.ExtraActivateMode = UpdateEvent.ExtraUpdateModes.Cancel;
            evt.UpdateType = UpdateTypes.ActorExtension;
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
        }

        void DieTask_OnAdditionStart(SmartEngine.Network.Map.Actor actor, Buff skill)
        {
            UpdateEvent evt = UpdateEvent.NewActorAdditionEvent(client.Character, client.Character, 255, 28674, 65010, UpdateEvent.ExtraUpdateModes.Activate);
            //evt.AddActorPara(Common.Packets.GameServer.PacketParameter.Speed, 6);
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);

            evt = new UpdateEvent();
            evt.Actor = client.Character;
            evt.Target = client.Character;
            evt.AdditionSession = 28674;
            evt.SkillSession = 0;
            evt.AdditionID = 65010;
            evt.RestTime = 80000;
            evt.UpdateType = UpdateTypes.ActorExtension;
            client.Map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, client.Character, true);
        }
    }
}
