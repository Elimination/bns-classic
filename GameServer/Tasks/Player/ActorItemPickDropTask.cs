﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Network.Client;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Player
{
    public class ActorItemPickDropTask : Buff
    {
        ActorPC pc;
        ActorExt item;
        ActionTypes action;
        public enum ActionTypes
        {
            Pick,
            Drop,
            PickCorpse,
        }
        public ActorItemPickDropTask(ActorPC pc, ActorExt item, ActionTypes action)
            : base(pc, "ActorItemPickDropTask", 400)
        {
            this.pc = pc;
            this.item = item;
            this.action = action;
            this.OnAdditionStart += new StartEventHandler(ActorItemPickDropTask_OnAdditionStart);
            this.OnAdditionEnd += new EndEventHandler(ActorItemPickDropTask_OnAdditionEnd);

            Task removed;
            if (pc.Tasks.TryRemove("ActorItemPickDropTask", out removed))
                removed.Deactivate();
        }

        void ActorItemPickDropTask_OnAdditionEnd(SmartEngine.Network.Map.Actor actor, Buff skill, bool cancel)
        {
            Task removed;
            Map.Map map = MapManager.Instance.GetMap(pc.MapInstanceID);
            
            pc.Tasks.TryRemove("ActorItemPickDropTask", out removed);
            switch (action)
            {
                case ActionTypes.Pick :
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemPick;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.UserData = (byte)1;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
                case ActionTypes.PickCorpse:
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemPickCorpse;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.UserData = (byte)1;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
                case ActionTypes.Drop:
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemDrop;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.X = (short)item.X;
                        evt.Y = (short)item.Y;
                        evt.Z = (short)item.Z;
                        evt.UserData = (byte)1;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
            }
        }

        void ActorItemPickDropTask_OnAdditionStart(SmartEngine.Network.Map.Actor actor, Buff skill)
        {
            Map.Map map = MapManager.Instance.GetMap(pc.MapInstanceID);
            switch (action)
            {
                case ActionTypes.Pick:
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemPick;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.UserData = (byte)0;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
                case ActionTypes.PickCorpse:
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemPickCorpse;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.UserData = (byte)0;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
                case ActionTypes.Drop:
                    {
                        UpdateEvent evt = new UpdateEvent();
                        evt.UpdateType = UpdateTypes.ItemDrop;
                        evt.Actor = pc;
                        evt.Target = item;
                        evt.X = (short)item.X;
                        evt.Y = (short)item.Y;
                        evt.Z = (short)item.Z;
                        evt.UserData = (byte)0;
                        map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, pc, true);
                    }
                    break;
            }
        }
    }
}
