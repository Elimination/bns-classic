﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Network.Client;

namespace SagaBNS.GameServer.Tasks.Player
{
    public class CombatStatusTask : Task
    {
        GameSession client;
        public CombatStatusTask(int duration, ActorPC actor)
            : base(duration, duration, "CombatStatusTask")
        {
            this.client = actor.Client();            
        }

        public override void CallBack()
        {
            this.Deactivate();
            if (client != null && client.Character != null)
            {
                Task removed;
                client.Character.Tasks.TryRemove("CombatStatusTask", out removed);
                client.ChangeCombatStatus(false);
            }
        }

    }
}
