﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Actor
{
    public class CorpseDeleteTask : Task
    {
        ActorCorpse actor;
        bool already = false;
        bool shoudDisappear = false;
        public CorpseDeleteTask(int disappearTime, ActorCorpse actor)
            : base(200, disappearTime, "CorpseDelete")
        {
            this.actor = actor;
            Task removed;
            if (actor.Tasks.TryRemove("CorpseDelete", out removed))
                removed.Deactivate();
            /*if (actor.NPC.BaseData.QuestIDs.Count > 0)
            {
                actor.QuestID = actor.NPC.BaseData.QuestIDs[0];
            }*/
            actor.Tasks["CorpseDelete"] = this;
        }

        public override void CallBack()
        {
            try
            {
                Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
                if (map == null)
                {
                    Task task;
                    actor.Tasks.TryRemove("CorpseDelete", out task);
                    this.Deactivate();
                }
                if (!already)
                {
                    already = true;
                    actor.Invisible = false;
                    map.OnActorVisibilityChange(actor);
                    map.DeleteActor(actor.NPC);
                }
                else
                {
                    if ((actor.AvailableItems.Count > 0) || actor.NPC.BaseData.QuestIDs.Count > 0)
                    {
                        if (!shoudDisappear)
                        {
                            shoudDisappear = true;
                            dueTime = 60000;
                            Activate();
                            actor.ShouldDisappear = true;
                            UpdateEvent evt = new UpdateEvent();
                            evt.Actor = actor;
                            evt.UpdateType = UpdateTypes.DeleteCorpse;
                            map.SendEventToAllActorsWhoCanSeeActor(MapEvents.EVENT_BROADCAST, evt, actor, false);

                        }
                        else
                        {
                            actor.Items = null;
                            actor.Gold = 0;
                            if (actor.CurrentPickingPlayer > 0)
                            {
                                ActorPC pc = map.GetActor(actor.CurrentPickingPlayer) as ActorPC;
                                if (pc != null)
                                {
                                    pc.Client().SendActorCorpseClose(actor.ActorID);
                                }
                            }
                            Task task;
                            actor.Tasks.TryRemove("CorpseDelete", out task);
                            this.Deactivate();
                            actor.ShouldDisappear = true;
                            map.DeleteActor(actor);
                        }
                    }
                    else
                    {
                        Task task;
                        actor.Tasks.TryRemove("CorpseDelete", out task);
                        this.Deactivate();
                        actor.ShouldDisappear = true;
                        map.DeleteActor(actor);
                    }
                }
            }
            catch (Exception ex)
            {
                SmartEngine.Core.Logger.ShowError(ex);
                Task task;
                actor.Tasks.TryRemove("CorpseDelete", out task);
                this.Deactivate();
            }
        }

    }
}
