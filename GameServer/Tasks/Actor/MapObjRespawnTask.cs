﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Actor
{
    public class MapObjRespawnTask : Task
    {
        ActorMapObj actor;
        bool already = false;
        public MapObjRespawnTask(ActorMapObj actor)
            : base(actor.RespawnTime, actor.RespawnTime, "Respawn")
        {
            this.actor = actor;
            Task removed;
            if (actor.Tasks.TryRemove("Respawn", out removed))
                removed.Deactivate();
            actor.Tasks["Respawn"] = this;
        }

        public override void CallBack()
        {
            this.Deactivate();
            Task task;
            actor.Tasks.TryRemove("Respawn", out task); 
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);

            actor.Available = true;
            actor.Items = null;
            UpdateEvent evt = new UpdateEvent();
            evt.Actor = actor;
            evt.UpdateType = UpdateTypes.MapObjectVisibilityChange;
            map.SendEventToAllActors(MapEvents.EVENT_BROADCAST, evt, actor, true);
        }

    }
}
