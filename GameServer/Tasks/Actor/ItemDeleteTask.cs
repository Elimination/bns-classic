﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Actor
{
    public class ItemDeleteTask : Task
    {
        ActorItem actor;
        public ItemDeleteTask(ActorItem actor)
            : base(actor.DisappearTime,actor.DisappearTime, "ItemDelete")
        {
            this.actor = actor;
            Task removed;
            if (actor.Tasks.TryRemove("ItemDelete", out removed))
                removed.Deactivate();
            /*if (actor.NPC.BaseData.QuestIDs.Count > 0)
            {
                actor.QuestID = actor.NPC.BaseData.QuestIDs[0];
            }*/
            actor.Tasks["ItemDelete"] = this;
        }

        public override void CallBack()
        {
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            Task task;
            actor.Tasks.TryRemove("ItemDelete", out task);
            this.Deactivate();
            map.DeleteActor(actor);
        }
    }
}
