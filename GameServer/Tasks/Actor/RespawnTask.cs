﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Tasks;
using SagaBNS.Common.Skills;
using SagaBNS.Common.Actors;
using SagaBNS.GameServer.Map;

namespace SagaBNS.GameServer.Tasks.Actor
{
    public class RespawnTask : Task
    {
        ActorNPC actor;
        bool already = false;
        public RespawnTask(int respawnTime, ActorNPC actor)
            : base(respawnTime, respawnTime, "Respawn")
        {
            this.actor = actor;
            Task removed;
            if (actor.Tasks.TryRemove("Respawn", out removed))
                removed.Deactivate();
            actor.Tasks["Respawn"] = this;
        }

        public override void CallBack()
        {
            this.Deactivate();
            Task task;
            actor.Tasks.TryRemove("Respawn", out task); 
            Map.Map map = Map.MapManager.Instance.GetMap(actor.MapInstanceID);
            if (map != null)
            {
                actor.X = actor.X_Ori;
                actor.Y = actor.Y_Ori;
                actor.Z = actor.Z_Ori;
                actor.Status.Dead = false;
                actor.HP = actor.MaxHP;
                map.RegisterActor(actor);
                actor.Invisible = false;
                map.OnActorVisibilityChange(actor);
                map.SendVisibleActorsToActor(actor);
            }            
        }

    }
}
