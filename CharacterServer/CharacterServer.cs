﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.VirtualFileSystem;
using SagaBNS.Common.Packets;
using SagaBNS.CharacterServer.Network.Client;
using SagaBNS.CharacterServer.Manager;

namespace SagaBNS.CharacterServer
{
    class CharacterServer
    {
        static DateTime upTime;
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(ShutingDown);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Logger.InitDefaultLogger("CharacterServer");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                        SagaBNS Character Server                ");
            Console.WriteLine("         (C)2010-2011 The BnS Emulator Project Development Team                ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Logger.ShowInfo("Version Informations:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("CharacterServer");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + VersionInformation.Version + "(" + VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Common Library");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SagaBNS.Common.VersionInformation.Version + "(" + SagaBNS.Common.VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("SmartEngine Network");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SmartEngine.Network.VersionInformation.Version + "(" + SmartEngine.Network.VersionInformation.ModifyDate + ")");
            Logger.ShowInfo("Initializing VirtualFileSystem...");
            VirtualFileSystemManager.Instance.Init(FileSystems.Real, ".");

            Logger.ShowInfo("Loading Configuration File...");
            Configuration.Instance.Initialization("./Config/CharacterServer.xml");
            //Logger.CurrentLogger.LogLevel.Value = Configuration.Instance.LogLevel;
            upTime = DateTime.Now;
            if (!StartDatabase())
            {
                Logger.ShowError("Cannot connect to Mysql server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            }

            CharacterClientManager.Instance.Port = Configuration.Instance.Port;
            if (!CharacterClientManager.Instance.Start())
            {
                Logger.ShowError("Cannot Listen on port:" + Configuration.Instance.Port);
                Logger.ShowError("Shutting down in 20sec.");
                CharacterClientManager.Instance.Stop();
                Database.CharacterDB.Instance.Shutdown();
                Database.ItemDB.Instance.Shutdown();
                System.Threading.Thread.Sleep(20000);
                return;
            }

            Logger.ShowInfo("Listening on port:" + CharacterClientManager.Instance.Port);
            Logger.ShowInfo("Accepting clients...");

            //处理Console命令
            while (true)
            {
                try
                {
                    string cmd = Console.ReadLine();
                    if (cmd == null)
                        break;
                    args = cmd.Split(' ');
                    switch (args[0].ToLower())
                    {
                        case "printthreads":
                            ClientManager.PrintAllThreads();
                            break;
                        case "printband":
                            int sendTotal = 0;
                            int receiveTotal = 0;
                            Logger.ShowWarning("Bandwidth usage information:");
                            foreach (Session<CharacterPacketOpcode> i in CharacterClientManager.Instance.Clients.ToArray())
                            {
                                try
                                {
                                    sendTotal += i.Network.UpStreamBand;
                                    receiveTotal += i.Network.DownStreamBand;
                                    Logger.ShowWarning(string.Format("Client:{0} Receive:{1:0.##}KB/s Send:{2:0.##}KB/s",
                                        i.ToString(),
                                        (float)i.Network.DownStreamBand / 1024,
                                        (float)i.Network.UpStreamBand / 1024));
                                }
                                catch { }
                            }
                            Logger.ShowWarning(string.Format("Total: Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                        (float)receiveTotal / 1024,
                                        (float)sendTotal / 1024));
                            break;
                        case "status":
                            PrintStatus();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }
        }

        static void PrintStatus()
        {
            Logger.ShowWarning("Running Status For Caches:");
            PrintCacheStatus<uint, Common.Actors.ActorPC>(Cache.CharacterCache.Instance, "CharacterCache");
            PrintCacheStatus<uint, Common.Item.Item>(Cache.ItemCache.Instance, "ItemCache");
            Logger.ShowWarning("Running Status For MySql DBs:");
            PrintMySQLStatus(Database.CharacterDB.Instance, "CharacterDB");
            PrintMySQLStatus(Database.ItemDB.Instance, "ItemDB");
        }

        static void PrintCacheStatus<K, T>(SmartEngine.Network.Database.Cache.Cache<K, T> cache, string name)
        {
            Logger.ShowWarning(string.Format("{0}:\r\n       Usage:{1}/{2} ({3:0.00}%)\r\n" +
                                               "       Requests/Min:{4:0.00} Hit Rate:{5:0.00}%\r\n" + 
                                               "       Write Requests/Min:{6:0.00} Avg Write Wait Time:{7}s\r\n" + 
                                               "       Old data clean-up triggered:{8:0.00}/Hour Avg clean-up trigger time:{9}"
                                               , name, cache.Count, cache.Capacity, (float)cache.Count * 100 / cache.Capacity,
                                               (float)cache.CacheRequestTimes / (DateTime.Now - upTime).TotalMinutes, 
                                               (float)cache.CacheRequestHitTimes * 100 / cache.CacheRequestTimes,
                                               (float)cache.WriteRequestTimes / (DateTime.Now - upTime).TotalMinutes, 
                                               cache.WriteAvgWaitingTime.TotalSeconds,
                                               (float)cache.EldestDataClearTimes / (DateTime.Now - upTime).TotalHours, cache.EldestDataClearAvgTime));
            
        }

        static void PrintMySQLStatus(SmartEngine.Network.Database.MySQLConnectivity sql, string name)
        {
            Logger.ShowWarning(string.Format("{0}:\r\n       Requests/Min:{1}  Execution/Min:{2}  Queue Length:{3}", name,sql.RequestsPerMinute, sql.ExecutionPerMinute, sql.QueueLength));
        }

        public static bool StartDatabase()
        {
            try
            {
                Database.CharacterDB.Instance.Init(Configuration.Instance.DBHost,
                                                 Configuration.Instance.DBPort,
                                                 Configuration.Instance.DBName,
                                                 Configuration.Instance.DBUser,
                                                 Configuration.Instance.DBPassword);
                Database.ItemDB.Instance.Init(Configuration.Instance.DBHost,
                                                 Configuration.Instance.DBPort,
                                                 Configuration.Instance.DBName,
                                                 Configuration.Instance.DBUser,
                                                 Configuration.Instance.DBPassword);
                return Database.CharacterDB.Instance.Connect() && Database.ItemDB.Instance.Connect();
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void ShutingDown(object sender, ConsoleCancelEventArgs args)
        {
            Logger.ShowInfo("Closing.....");
            CharacterClientManager.Instance.Stop();
            Cache.CharacterCache.Instance.Shutdown();
            Database.CharacterDB.Instance.Shutdown();
            Cache.ItemCache.Instance.Shutdown();
            Database.ItemDB.Instance.Shutdown();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            Logger.ShowError("Fatal: An unhandled exception is thrown, terminating...");
            Logger.ShowError("Error Message:" + ex.Message);
            Logger.ShowError("Call Stack:" + ex.StackTrace);
            Logger.ShowError("Trying to save all player's data");

            CharacterClientManager.Instance.Stop();
            Cache.CharacterCache.Instance.Shutdown();
            Database.CharacterDB.Instance.Shutdown();
            Cache.ItemCache.Instance.Shutdown();
            Database.ItemDB.Instance.Shutdown();
        }
    }
}
