﻿/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50149
Source Host           : localhost:3306
Source Database       : sagabns

Target Server Type    : MYSQL
Target Server Version : 50149
File Encoding         : 65001

Date: 2012-09-13 10:03:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `char`
-- ----------------------------
DROP TABLE IF EXISTS `char`;
CREATE TABLE `char` (
  `char_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `world_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `slot_id` tinyint(3) unsigned NOT NULL,
  `race` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(3) unsigned NOT NULL,
  `job` tinyint(3) unsigned NOT NULL,
  `appearence` varchar(50) NOT NULL,
  `appearence2` varchar(256) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `exp` int(10) unsigned NOT NULL,
  `hp` smallint(5) unsigned NOT NULL,
  `mp` smallint(5) unsigned NOT NULL,
  `max_hp` smallint(6) unsigned NOT NULL,
  `max_mp` smallint(6) unsigned NOT NULL,
  `gold` int(10) unsigned NOT NULL,
  `map_id` int(10) unsigned NOT NULL,
  `x` smallint(5) NOT NULL,
  `y` smallint(5) NOT NULL,
  `z` smallint(5) NOT NULL,
  `dir` smallint(5) unsigned NOT NULL,
  `ui_settings` varchar(512) NOT NULL DEFAULT '',
  `inventory_size` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`char_id`),
  UNIQUE KEY `name` (`name`,`world_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of char
-- ----------------------------

-- ----------------------------
-- Table structure for `item`
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(10) unsigned NOT NULL,
  `char_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `slot_id` smallint(5) unsigned NOT NULL,
  `container` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` smallint(5) unsigned NOT NULL DEFAULT '1',
  `synthesis` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `charid` (`char_id`),
  CONSTRAINT `charid` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item
-- ----------------------------

-- ----------------------------
-- Table structure for `quest`
-- ----------------------------
DROP TABLE IF EXISTS `quest`;
CREATE TABLE `quest` (
  `quest_id` smallint(5) unsigned NOT NULL,
  `char_id` int(10) unsigned NOT NULL,
  `step` tinyint(3) unsigned NOT NULL,
  `step_status` tinyint(3) unsigned NOT NULL,
  `next_step` tinyint(3) unsigned NOT NULL,
  `flag1` smallint(6) NOT NULL,
  `flag2` smallint(6) NOT NULL,
  `flag3` smallint(6) NOT NULL,
  `count1` int(11) NOT NULL DEFAULT '0',
  `count2` int(11) NOT NULL DEFAULT '0',
  `count3` int(11) NOT NULL DEFAULT '0',
  `count4` int(11) NOT NULL DEFAULT '0',
  `count5` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`char_id`),
  CONSTRAINT `id` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quest
-- ----------------------------

-- ----------------------------
-- Table structure for `quest_completed`
-- ----------------------------
DROP TABLE IF EXISTS `quest_completed`;
CREATE TABLE `quest_completed` (
  `char_id` int(10) unsigned NOT NULL,
  `quest_id` smallint(5) unsigned NOT NULL,
  KEY `qcCharID` (`char_id`),
  CONSTRAINT `qcCharID` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quest_completed
-- ----------------------------

-- ----------------------------
-- Table structure for `skills`
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `char_id` int(10) unsigned NOT NULL,
  `skill_id` int(10) unsigned NOT NULL,
  KEY `char_id` (`char_id`),
  CONSTRAINT `char_id` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skills
-- ----------------------------

-- ----------------------------
-- Table structure for `teleport_locations`
-- ----------------------------
DROP TABLE IF EXISTS `teleport_locations`;
CREATE TABLE `teleport_locations` (
  `char_id` int(10) unsigned NOT NULL,
  `teleport_id` smallint(5) unsigned NOT NULL,
  KEY `char_id` (`char_id`),
  CONSTRAINT `TcharId` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teleport_locations
-- ----------------------------
