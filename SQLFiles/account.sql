/*
MySQL Data Transfer
Source Host: localhost
Source Database: sagabns
Target Host: localhost
Target Database: sagabns
Date: 2012/9/2 7:34:16
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for account
-- ----------------------------
CREATE TABLE `account` (
  `account_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `extra_slot` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `gm_lv` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '',
  `last_login_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`account_id`),
  KEY `account` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
