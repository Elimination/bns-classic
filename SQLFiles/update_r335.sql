DROP TABLE IF EXISTS `teleport_locations`;
CREATE TABLE `teleport_locations` (
  `char_id` int(10) unsigned NOT NULL,
  `teleport_id` smallint(5) unsigned NOT NULL,
  KEY `char_id` (`char_id`),
  CONSTRAINT `TcharId` FOREIGN KEY (`char_id`) REFERENCES `char` (`char_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `char` ADD `ui_settings` varchar(1024) NOT NULL DEFAULT '';
