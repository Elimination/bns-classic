﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LaunchHelper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = Program.path;            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (System.IO.File.Exists(FD.SelectedPath + "\\bin\\Client.exe"))
                {
                    textBox1.Text = FD.SelectedPath;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(Program.startup + "\\path.txt", false, Encoding.Unicode);
                    sw.WriteLine(textBox1.Text);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    MessageBox.Show("请选择正确的游戏安装目录", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);                
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists(textBox1.Text + "\\bin\\Client.exe"))
            {
                MessageBox.Show("请选择正确的游戏安装目录", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string arg = string.Format("/LaunchByLauncher /SessKey:{0} /MacAddr:{1} /UserNick:{2} /CompanyID:{3} /ChannelGroupIndex:-1 /ServerAddr:  /StartGameID:{4} /RepositorySub:  /GamePath:{5}",
                Program.session, Program.mac, Program.nick, Program.company, Program.game, textBox1.Text);
            System.Diagnostics.Process process = System.Diagnostics.Process.Start(textBox1.Text + "\\bin\\Client.exe", arg);
            this.Close();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = System.Diagnostics.Process.Start(Program.startup + "\\PlayNCLauncher_ori.exe", Program.arg);
            this.Close();
        }
    }
}
