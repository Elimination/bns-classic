﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LaunchHelper
{
    static class Program
    {
        public static string session, nick, mac, company, game, path, arg, startup;
            
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] arg)
        {
            Program.arg = "";
            bool ok = false;
            foreach (string i in arg)
            {
                if (i.ToLower() == "waigame")
                {
                    ok = true;
                    continue;
                }
                Program.arg += " " + i;
                startup = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                if (i.Contains("/SessKey:"))
                {
                    session = i.Replace("/SessKey:", "");
                }
                if (i.Contains("/UserNick:"))
                {
                    nick = i.Replace("/UserNick:", "");
                }
                if (i.Contains("/MacAddr:"))
                {
                    mac = i.Replace("/MacAddr:", "");
                }
                if (i.Contains("/CompanyID:"))
                {
                    company = i.Replace("/CompanyID:", "");
                }
                if (i.Contains("/StartGameID:"))
                {
                    game = i.Replace("/StartGameID:", "");
                }
            }
            if (!ok)
                return;
            if (System.IO.File.Exists(startup + "\\path.txt"))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(startup + "\\path.txt", System.Text.Encoding.Unicode);
                path = sr.ReadLine();
                sr.Close();
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
