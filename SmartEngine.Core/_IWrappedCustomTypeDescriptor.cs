﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartEngine.Core
{
    public interface _IWrappedCustomTypeDescriptor
    {
        object GetWrapperOwner();
    }
}
