﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartEngine.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LogicSystemCallStaticOverInstanceAttribute : Attribute
    {
    }
}
