﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.LobbyServer.Network.Client;

namespace SagaBNS.LobbyServer.Packets.Client
{
    public class SM_REQUEST_LOGIN : Packet<LobbyPacketOpcode>
    {
        public SM_REQUEST_LOGIN()
        {
            this.ID = LobbyPacketOpcode.SM_REQUEST_LOGIN;
        }

        public ulong CharID
        {
            set
            {
                PutULong(value, 2);
            }
        }
    }
}
