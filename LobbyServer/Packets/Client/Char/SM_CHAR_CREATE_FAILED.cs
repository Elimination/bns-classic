﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.LobbyServer.Network.Client;

namespace SagaBNS.LobbyServer.Packets.Client
{
    public class SM_CHAR_CREATE_FAILED : Packet<LobbyPacketOpcode>
    {
        public enum Reasons
        {
            Failed = 111,
            InvalidName = 113,
            NameAlreadyExists
        }
        public SM_CHAR_CREATE_FAILED()
        {
            this.ID = LobbyPacketOpcode.SM_CHAR_CREATE_FAILED;
        }

        public byte[] SlotID
        {
            set
            {
                PutBytes(value, 2);
            }
        }

        public Reasons Reason
        {
            set
            {
                PutShort((short)value, 18);
            }
        }
    }
}
