﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.LobbyServer.Network.Client;

namespace SagaBNS.LobbyServer.Packets.Client
{
    public class CM_AUTH : Packet<LobbyPacketOpcode>
    {
        public CM_AUTH()
        {
            this.ID = LobbyPacketOpcode.CM_AUTH;
        }

        public Guid Token
        {
            get
            {
                return new Guid(GetBytes(16, 2));
            }
        }

        public Guid AccountID
        {
            get
            {
                return new Guid(GetBytes(16, 18));
            }
        }

        public override Packet<LobbyPacketOpcode> New()
        {
            return new CM_AUTH();
        }

        public override void OnProcess(Session<LobbyPacketOpcode> client)
        {
            ((LobbySession)client).OnLoginAuth(this);
        }
    }
}
