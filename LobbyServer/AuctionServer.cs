﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.VirtualFileSystem;
using SmartEngine.Core;

using SagaBNS.AuctionServer.Packets;
using SagaBNS.AuctionServer.Manager;
using SagaBNS.Common.Packets;

namespace SagaBNS.AuctionServer
{
    class AuctionServer
    {
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(ShutingDown);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Logger.InitDefaultLogger("AuctionServer");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                         SagaBNS Auction Server                ");
            Console.WriteLine("         (C)2010-2011 The BnS Emulator Project Development Team                ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Logger.ShowInfo("Version Informations:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Common Library");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SagaBNS.Common.VersionInformation.Version + "(" + SagaBNS.Common.VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("SmartEngine Network");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SmartEngine.Network.VersionInformation.Version + "(" + SmartEngine.Network.VersionInformation.ModifyDate + ")");
            Logger.ShowInfo("Initializing VirtualFileSystem...");
            VirtualFileSystemManager.Instance.Init(FileSystems.Real, ".");

            Logger.ShowInfo("Loading Configuration File...");
            Configuration.Instance.Initialization("./Config/AuctionServer.xml");

            Network<AuctionPacketOpcode>.SuppressUnknownPackets = !Debugger.IsAttached;
           
            AuctionClientManager.Instance.Port = Configuration.Instance.Port;
            Encryption.Implementation = new SagaBNS.Common.Encryption.BNSAESEncryption();
            Network<AuctionPacketOpcode>.Implementation = new SagaBNS.Common.BNSGameNetwork<AuctionPacketOpcode>();
            if (!AuctionClientManager.Instance.Start())
            {
                Logger.ShowError("Cannot Listen on port:" + Configuration.Instance.Port);
                Logger.ShowError("Shutting down in 20sec.");
                AuctionClientManager.Instance.Stop();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }

            Logger.ShowInfo("Listening on port:" + AuctionClientManager.Instance.Port);
            Logger.ShowInfo("Accepting clients...");

            //处理Console命令
            while (true)
            {
                try
                {
                    string cmd = Console.ReadLine();
                    if (cmd == null)
                        break;
                    args = cmd.Split(' ');
                    switch (args[0].ToLower())
                    {
                        case "printband":
                            int sendTotal = 0;
                            int receiveTotal = 0;
                            Logger.ShowWarning("Bandwidth usage information:");
                            try
                            {
                                foreach (Session<AuctionPacketOpcode> i in AuctionClientManager.Instance.Clients.ToArray())
                                {
                                    sendTotal += i.Network.UpStreamBand;
                                    receiveTotal += i.Network.DownStreamBand;
                                    Logger.ShowWarning(string.Format("Client:{0} Receive:{1:0.##}KB/s Send:{2:0.##}KB/s",
                                        i.ToString(),
                                        (float)i.Network.DownStreamBand / 1024,
                                        (float)i.Network.UpStreamBand / 1024));
                                }
                            }
                            catch { }
                            Logger.ShowWarning(string.Format("Total: Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                        (float)receiveTotal / 1024,
                                        (float)sendTotal / 1024));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }

        }

        private static void ShutingDown(object sender, ConsoleCancelEventArgs args)
        {
            Logger.ShowInfo("Closing.....");
            AuctionClientManager.Instance.Stop();
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            Logger.ShowError("Fatal: An unhandled exception is thrown, terminating...");
            Logger.ShowError("Error Message:" + ex.Message);
            Logger.ShowError("Call Stack:" + ex.StackTrace);
            Logger.ShowError("Trying to save all player's data");

            AuctionClientManager.Instance.Stop();
        }
    }
}
