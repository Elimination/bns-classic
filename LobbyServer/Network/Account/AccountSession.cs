﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartEngine.Core;
using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Network;
using SagaBNS.LobbyServer.Network.Client;

namespace SagaBNS.LobbyServer.Network.AccountServer
{
    public partial class AccountSession : AccountSession<LobbySession>
    {
        static AccountSession instance = new AccountSession();
        public static AccountSession Instance { get { return instance; } }

        public AccountSession()
        {
            this.Host = Configuration.Instance.AccountHost;
            this.Port = Configuration.Instance.AccountPort;
            this.AccountPassword = Configuration.Instance.AccountPassword;
        }

        protected override void OnAccountLoginResult(LobbySession client, Common.Packets.AccountServer.AccountLoginResult result)
        {
            
        }

        protected override void OnAccountLogoutNotify(uint accountID)
        {
            
        }

        protected override void OnAccountInfo(LobbySession client, Common.Packets.AccountServer.AccountLoginResult result, Common.Account.Account acc)
        {
            client.OnGotAccountInfo(result, acc);
        }
    }
}
