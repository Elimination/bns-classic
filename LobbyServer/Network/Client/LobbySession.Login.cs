﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.Map;
using SmartEngine.Network.Utils;
using SagaBNS.Common;
using SagaBNS.Common.Actors;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Account;
using SagaBNS.Common.Network;
using SagaBNS.LobbyServer.Manager;
using SagaBNS.LobbyServer.Packets.Client;
using SagaBNS.LobbyServer.Network.AccountServer;
namespace SagaBNS.LobbyServer.Network.Client
{
    public partial class LobbySession : Session<LobbyPacketOpcode>
    {
        Guid accountID;
        Account acc;
        public void OnLoginAuth(CM_AUTH p)
        {
            accountID = p.AccountID;
            Logger.ShowInfo(string.Format("Account:{0} is loging in", p.AccountID));
            AccountSession.Instance.RequestAccountInfo(accountID.ToUInt(), this);
        }

        public void OnGotAccountInfo(Common.Packets.AccountServer.AccountLoginResult result, Account acc)
        {
            Logger.ShowInfo(string.Format("Load Account info for {0}({1}):{2}", accountID, accountID.ToUInt(), result));
            if (result == Common.Packets.AccountServer.AccountLoginResult.OK)
            {
                this.acc = acc;
                SM_AUTH_RESULT p1 = new SM_AUTH_RESULT();
                this.Network.SendPacket(p1);
            }
        }

        public void OnWorldListRequest(CM_SERVER_LIST p)
        {
            SM_SERVER_LIST p1 = new SM_SERVER_LIST();
            p1.Worlds = WorldManager.Instance.Worlds.Values;
            this.Network.SendPacket(p1);
        }
    }
}
