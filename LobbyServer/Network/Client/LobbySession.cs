﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartEngine.Core;
using SmartEngine.Network;
using SagaBNS.Common.Packets;

namespace SagaBNS.LobbyServer.Network.Client
{
    public partial class LobbySession : Session<LobbyPacketOpcode>
    {
        public override void OnConnect()
        {
            base.OnConnect();
        }

        public override void OnDisconnect()
        {
            base.OnDisconnect();
        }

        public override string ToString()
        {
                return this.Network.Socket.RemoteEndPoint.ToString();
        }
    }
}
