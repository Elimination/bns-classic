﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SmartEngine.Network.VirtualFileSystem;
using SmartEngine.Core;

using SagaBNS.LobbyServer.Packets;
using SagaBNS.LobbyServer.Manager;
using SagaBNS.Common.Packets;
using SagaBNS.Common.Network;
using SagaBNS.LobbyServer.Network.AccountServer;
using SagaBNS.LobbyServer.Network.CharacterServer;

namespace SagaBNS.LobbyServer
{
    class LobbyServer
    {
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(ShutingDown);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Logger.InitDefaultLogger("LobbyServer");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                         SagaBNS Lobby Server                ");
            Console.WriteLine("         (C)2010-2011 The BnS Emulator Project Development Team                ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Logger.ShowInfo("Version Informations:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Common Library");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SagaBNS.Common.VersionInformation.Version + "(" + SagaBNS.Common.VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("SmartEngine Network");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SmartEngine.Network.VersionInformation.Version + "(" + SmartEngine.Network.VersionInformation.ModifyDate + ")");
            Logger.ShowInfo("Initializing VirtualFileSystem...");
            VirtualFileSystemManager.Instance.Init(FileSystems.Real, ".");

            Logger.ShowInfo("Loading Configuration File...");
            Configuration.Instance.Initialization("./Config/LobbyServer.xml");

            Network<LobbyPacketOpcode>.SuppressUnknownPackets = !Debugger.IsAttached;

            Logger.ShowInfo(string.Format("Connecting account server at {0}:{1}", Configuration.Instance.AccountHost, Configuration.Instance.AccountPort));
            if (!AccountSession.Instance.Connect(5))
            {
                Logger.ShowError("Cannot connect to account server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            } 
            while (AccountSession.Instance.State == SESSION_STATE.NOT_IDENTIFIED ||
                AccountSession.Instance.State == SESSION_STATE.CONNECTED ||
                AccountSession.Instance.State == SESSION_STATE.DISCONNECTED)
            {
                System.Threading.Thread.Sleep(100);
            }
            if (AccountSession.Instance.State == SESSION_STATE.REJECTED ||
                AccountSession.Instance.State == SESSION_STATE.FAILED)
            {
                if (AccountSession.Instance.State == SESSION_STATE.REJECTED)
                    Logger.ShowError("Account server refused login request, please check the password");
                Logger.ShowInfo("Shutting down in 20sec.");
                AccountSession.Instance.Network.Disconnect();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }
            Logger.ShowInfo("Login to account server successful");

            Logger.ShowInfo(string.Format("Connecting character server at {0}:{1}", Configuration.Instance.CharacterHost, Configuration.Instance.CharacterPort));
            if (!CharacterSession.Instance.Connect(5))
            {
                Logger.ShowError("Cannot connect to character server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            }
            while (CharacterSession.Instance.State == SESSION_STATE.NOT_IDENTIFIED ||
                CharacterSession.Instance.State == SESSION_STATE.CONNECTED ||
                CharacterSession.Instance.State == SESSION_STATE.DISCONNECTED)
            {
                System.Threading.Thread.Sleep(100);
            }
            if (CharacterSession.Instance.State == SESSION_STATE.REJECTED ||
                CharacterSession.Instance.State == SESSION_STATE.FAILED)
            {
                if (CharacterSession.Instance.State == SESSION_STATE.REJECTED)
                    Logger.ShowError("Character server refused login request, please check the password");
                Logger.ShowInfo("Shutting down in 20sec.");
                AccountSession.Instance.Network.Disconnect();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }
            Logger.ShowInfo("Login to character server successful");

            
            LobbyClientManager.Instance.Port = Configuration.Instance.Port;
            Encryption.Implementation = new SagaBNS.Common.Encryption.BNSAESEncryption();
            Network<LobbyPacketOpcode>.Implementation = new SagaBNS.Common.BNSGameNetwork<LobbyPacketOpcode>();
            if (!LobbyClientManager.Instance.Start())
            {
                Logger.ShowError("Cannot Listen on port:" + Configuration.Instance.Port);
                Logger.ShowError("Shutting down in 20sec.");
                LobbyClientManager.Instance.Stop();
                System.Threading.Thread.Sleep(20000);
                Environment.Exit(0);
                return;
            }

            Logger.ShowInfo("Listening on port:" + LobbyClientManager.Instance.Port);
            Logger.ShowInfo("Accepting clients...");

            //处理Console命令
            while (true)
            {
                try
                {
                    string cmd = Console.ReadLine();
                    if (cmd == null)
                        break;
                    args = cmd.Split(' ');
                    switch (args[0].ToLower())
                    {
                        case "printband":
                            int sendTotal = 0;
                            int receiveTotal = 0;
                            Logger.ShowWarning("Bandwidth usage information:");
                            try
                            {
                                foreach (Session<LobbyPacketOpcode> i in LobbyClientManager.Instance.Clients.ToArray())
                                {
                                    sendTotal += i.Network.UpStreamBand;
                                    receiveTotal += i.Network.DownStreamBand;
                                    Logger.ShowWarning(string.Format("Client:{0} Receive:{1:0.##}KB/s Send:{2:0.##}KB/s",
                                        i.ToString(),
                                        (float)i.Network.DownStreamBand / 1024,
                                        (float)i.Network.UpStreamBand / 1024));
                                }
                            }
                            catch { }
                            Logger.ShowWarning(string.Format("Total: Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                        (float)receiveTotal / 1024,
                                        (float)sendTotal / 1024));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }

        }

        private static void ShutingDown(object sender, ConsoleCancelEventArgs args)
        {
            Logger.ShowInfo("Closing.....");
            LobbyClientManager.Instance.Stop();
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            Logger.ShowError("Fatal: An unhandled exception is thrown, terminating...");
            Logger.ShowError("Error Message:" + ex.Message);
            Logger.ShowError("Call Stack:" + ex.StackTrace);
            Logger.ShowError("Trying to save all data");

            LobbyClientManager.Instance.Stop();
        }
    }
}
