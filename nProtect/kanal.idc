#include <idc.idc>

static main(void)
{

	auto slotidx;
	slotidx = 1;

	MarkPosition(0x006030B8, 0, 0, 0, slotidx + 0, "BASE64 table");
	MakeComm(PrevNotTail(0x006030B9), "BASE64 table\nBASE64 encoding (used e.g. in e-mails - MIME)");
	MarkPosition(0x005C2378, 0, 0, 0, slotidx + 1, "CRC32");
	MakeComm(PrevNotTail(0x005C2379), "CRC32\nCRC32 precomputed table for byte transform");
	MarkPosition(0x008D114E, 0, 0, 0, slotidx + 2, "CryptCreateHash [Name]");
	MakeComm(PrevNotTail(0x008D114F), "CryptCreateHash [Name]\nMicrosoft CryptoAPI function name");
	MarkPosition(0x008D113C, 0, 0, 0, slotidx + 3, "CryptDeriveKey [Name]");
	MakeComm(PrevNotTail(0x008D113D), "CryptDeriveKey [Name]\nMicrosoft CryptoAPI function name");
	MarkPosition(0x008D1208, 0, 0, 0, slotidx + 4, "CryptEncrypt [Name]");
	MakeComm(PrevNotTail(0x008D1209), "CryptEncrypt [Name]\nMicrosoft CryptoAPI function name");
	MarkPosition(0x008D1160, 0, 0, 0, slotidx + 5, "CryptHashData [Name]");
	MakeComm(PrevNotTail(0x008D1161), "CryptHashData [Name]\nMicrosoft CryptoAPI function name");
	MarkPosition(0x005F7250, 0, 0, 0, slotidx + 6, "List of primes [long]");
	MakeComm(PrevNotTail(0x005F7251), "List of primes [long]\nAll prime numbers up to 251");
	MarkPosition(0x005F4E28, 0, 0, 0, slotidx + 7, "RIJNDAEL [T1]");
	MakeComm(PrevNotTail(0x005F4E29), "RIJNDAEL [T1]\nRIJNDAEL (AES): encoding table 1; also used in other ciphers (e.g. SNOW 2.0)");
	MarkPosition(0x00577B76, 0, 0, 0, slotidx + 8, "RIPEMD-128");
	MakeComm(PrevNotTail(0x00577B77), "RIPEMD-128\nRIPEMD-128/256 additive constants (also used by other RIPEMD variants, partly in SHA, MD4, SEAL, ...)");
	MarkPosition(0x00574F12, 0, 0, 0, slotidx + 9, "RIPEMD-160");
	MakeComm(PrevNotTail(0x00574F13), "RIPEMD-160\nRIPEMD-160/320 additive constants (partly also used by other RIPEMD variants, SHA, MD4, SEAL, ...)");
	MarkPosition(0x00680B74, 0, 0, 0, slotidx + 10, "SEED");
	MakeComm(PrevNotTail(0x00680B75), "SEED\nSEED: SBOX 0");
	MarkPosition(0x00575AF0, 0, 0, 0, slotidx + 11, "SHA1/RIPEMD-160 [Init]");
	MakeComm(PrevNotTail(0x00575AF1), "SHA1/RIPEMD-160 [Init]\nSHA/RIPEMD init constants (partly used also in MD4/5 and some others)");
	MarkPosition(0x006834B0, 0, 0, 0, slotidx + 12, "TWOFISH [8x8]");
	MakeComm(PrevNotTail(0x006834B1), "TWOFISH [8x8]\nTWOFISH: pregenerated 8x8 Sbox table");
	MarkPosition(0x005C2278, 0, 0, 0, slotidx + 13, "ZLIB deflate [word]");
	MakeComm(PrevNotTail(0x005C2279), "ZLIB deflate [word]\nZLIB compression algorithm - literal codes base values, used to build the trees");
}

