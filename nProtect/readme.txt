/*
Fyyre
http://fyyre.l2-fashion.de/

DStuff
http://dstuff.l2wh.com/
*/


nProtectDec, v0.1

All 'interesting' strings within nProtect binaries are encoded in order to hide them from causal
string search.

This tool decodeds these strings into un-encoded, readable string =)

Usage... nProtectDec.exe filename decoded.txt rejects.txt

filename <-- name of nProtect binary, must be unpacked or dumped from memory.

decoded.txt <<-- decoded strings are output into this file

rejects.txt <<-- junk output, unmatched strings.


disclaimer: responsibility, cause & effect from use of this tool, is yours.  not ours.