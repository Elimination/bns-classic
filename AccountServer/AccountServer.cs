﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network.Utils;
using SmartEngine.Network;
using SmartEngine.Network.VirtualFileSystem;

using SagaBNS.Common.Packets;
using SagaBNS.AccountServer.Manager;

namespace SagaBNS.AccountServer
{
    class AccountServer
    {
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(ShutingDown);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Logger.InitDefaultLogger("AccountServer");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("                         SagaBNS Account Server                ");
            Console.WriteLine("         (C)2010-2011 The BnS Emulator Project Development Team                ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================================================================");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Logger.ShowInfo("Version Informations:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("AccountServer");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + VersionInformation.Version + "(" + VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Common Library");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SagaBNS.Common.VersionInformation.Version + "(" + SagaBNS.Common.VersionInformation.ModifyDate + ")");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("SmartEngine Network");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(":SVN Rev." + SmartEngine.Network.VersionInformation.Version + "(" + SmartEngine.Network.VersionInformation.ModifyDate + ")");
            Logger.ShowInfo("Initializing VirtualFileSystem...");
            VirtualFileSystemManager.Instance.Init(FileSystems.Real, ".");

            Logger.ShowInfo("Loading Configuration File...");
            Configuration.Instance.Initialization("./Config/AccountServer.xml");
            //Logger.CurrentLogger.LogLevel.Value = Configuration.Instance.LogLevel;

            if (!StartDatabase())
            {
                Logger.ShowError("Cannot connect to Mysql server");
                Logger.ShowError("Shutting down in 20sec.");
                System.Threading.Thread.Sleep(20000);
                return;
            }

            AccountClientManager.Instance.Port = Configuration.Instance.Port;
            if (!AccountClientManager.Instance.Start())
            {
                Logger.ShowError("Cannot Listen on port:" + Configuration.Instance.Port);
                Logger.ShowError("Shutting down in 20sec.");
                AccountClientManager.Instance.Stop();
                Database.AccountDB.Instance.Shutdown();
                System.Threading.Thread.Sleep(20000);
                return;
            }

            Logger.ShowInfo("Listening on port:" + AccountClientManager.Instance.Port);
            Logger.ShowInfo("Accepting clients...");

            //处理Console命令
            while (true)
            {
                try
                {
                    string cmd = Console.ReadLine();
                    if (cmd == null)
                        break;
                    args = cmd.Split(' ');
                    switch (args[0].ToLower())
                    {
                        case "printthreads":
                            ClientManager.PrintAllThreads();
                            break;
                        case "printband":
                            int sendTotal = 0;
                            int receiveTotal = 0;
                            Logger.ShowWarning("Bandwidth usage information:");
                            try
                            {
                                foreach (Session<AccountPacketOpcode> i in AccountClientManager.Instance.Clients.ToArray())
                                {
                                    sendTotal += i.Network.UpStreamBand;
                                    receiveTotal += i.Network.DownStreamBand;
                                    Logger.ShowWarning(string.Format("Client:{0} Receive:{1:0.##}KB/s Send:{2:0.##}KB/s",
                                        i.ToString(),
                                        (float)i.Network.DownStreamBand / 1024,
                                        (float)i.Network.UpStreamBand / 1024));
                                }
                            }
                            catch { }
                            Logger.ShowWarning(string.Format("Total: Receive:{0:0.##}KB/s Send:{1:0.##}KB/s",
                                        (float)receiveTotal / 1024,
                                        (float)sendTotal / 1024));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ShowError(ex);
                }
            }
        }

        public static bool StartDatabase()
        {
            try
            {
                Database.AccountDB.Instance.Init(Configuration.Instance.DBHost,
                                                 Configuration.Instance.DBPort,
                                                 Configuration.Instance.DBName,
                                                 Configuration.Instance.DBUser,
                                                 Configuration.Instance.DBPassword);
                return Database.AccountDB.Instance.Connect();
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void ShutingDown(object sender, ConsoleCancelEventArgs args)
        {
            Logger.ShowInfo("Closing.....");
            AccountClientManager.Instance.Stop();
            Database.AccountDB.Instance.Shutdown();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            Logger.ShowError("Fatal: An unhandled exception is thrown, terminating...");
            Logger.ShowError("Error Message:" + ex.Message);
            Logger.ShowError("Call Stack:" + ex.StackTrace);
            Logger.ShowError("Trying to save all player's data");

            AccountClientManager.Instance.Stop();
            Database.AccountDB.Instance.Shutdown();
        }
    }
}
