﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.Utils;

namespace SagaBNS.AccountServer
{
    public class Configuration : Singleton<Configuration>
    {
        string dbhost, dbuser, dbpass, dbname, language, password;
        int dbport, port, loglevel;

        /// <summary>
        /// Mysql服务器地址
        /// </summary>
        public string DBHost { get { return this.dbhost; } }

        /// <summary>
        /// Mysql服务器端口
        /// </summary>
        public int DBPort { get { return this.dbport; } }

        /// <summary>
        /// Mysql数据库用户名
        /// </summary>
        public string DBUser { get { return this.dbuser; } }

        /// <summary>
        /// Mysql数据库密码
        /// </summary>
        public string DBPassword { get { return this.dbpass; } }

        /// <summary>
        /// Mysql数据库名称
        /// </summary>
        public string DBName { get { return this.dbname; } }

        /// <summary>
        /// 内部通讯连接密码
        /// </summary>
        public string Password { get { return this.password; } }

        /// <summary>
        /// 监听端口
        /// </summary>
        public int Port { get { return this.port; } }

        /// <summary>
        /// 日志等级
        /// </summary>
        public int LogLevel { get { return this.loglevel; } }

        public void Initialization(string path)
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                XmlElement root;
                XmlNodeList list;
                xml.Load(path);
                root = xml["AccountServer"];
                list = root.ChildNodes;
                foreach (object j in list)
                {
                    XmlElement i;
                    if (j.GetType() != typeof(XmlElement)) continue;
                    i = (XmlElement)j;
                    switch (i.Name.ToLower())
                    {
                        case "port":
                            this.port = int.Parse(i.InnerText);
                            break;
                        case "dbhost":
                            this.dbhost = i.InnerText;
                            break;
                        case "dbport":
                            this.dbport = int.Parse(i.InnerText);
                            break;
                        case "dbuser":
                            this.dbuser = i.InnerText;
                            break;
                        case "dbpass":
                            this.dbpass = i.InnerText;
                            break;
                        case "dbname":
                            this.dbname = i.InnerText;
                            break;
                        case "password":
                            this.password = i.InnerText;
                            break;
                        case "loglevel":
                            this.loglevel = int.Parse(i.InnerText);
                            break;
                    }
                }
                Logger.ShowInfo("Done reading configuration...");
                xml = null;
            }
            catch (Exception ex)
            {
                Logger.ShowError(ex);
            }
        }
    }
}
