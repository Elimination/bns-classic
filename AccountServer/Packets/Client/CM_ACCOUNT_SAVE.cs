﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.AccountServer.Network.Client;

namespace SagaBNS.AccountServer.Packets.Client
{
    public class CM_ACCOUNT_SAVE : SagaBNS.Common.Packets.AccountServer.CM_ACCOUNT_SAVE
    {
        public override Packet<AccountPacketOpcode> New()
        {
            return new CM_ACCOUNT_SAVE();
        }

        public override void OnProcess(Session<AccountPacketOpcode> client)
        {
            ((AccountSession)client).OnAccountSave(this);
        }
    }
}
