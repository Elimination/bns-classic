﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.Utils;

using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.AccountServer;
using SagaBNS.Common.Account;
using SagaBNS.AccountServer.Cache;
using SagaBNS.AccountServer.Database;
using SagaBNS.AccountServer.Manager;
namespace SagaBNS.AccountServer.Network.Client
{
    public partial class AccountSession : Session<AccountPacketOpcode>
    {
        public void OnAccountInfoRequest(CM_ACCOUNT_INFO_REQUEST p)
        {
            SM_ACCOUNT_INFO p1 = new SM_ACCOUNT_INFO();
            p1.SessionID = p.SessionID;

            uint accountID;
            Logger.ShowInfo(string.Format(this + ":Player:{0} is trying to login", p.Username));
            AccountLoginResult res = AccountDB.Instance.GetAccountID(p.Username, out accountID);
            switch (res)
            {
                case AccountLoginResult.OK:
                    p1.Result = AccountLoginResult.OK;
                    p1.Account = AccountCache.Instance[accountID];
                    break;
                case AccountLoginResult.NO_SUCH_ACCOUNT:
                    p1.Result = AccountLoginResult.NO_SUCH_ACCOUNT;
                    break;
                case AccountLoginResult.DB_ERROR:
                    p1.Result = AccountLoginResult.DB_ERROR;
                    break;
            }
            Logger.ShowInfo(string.Format("Login result:{0}", res));
            this.Network.SendPacket(p1);
        }

        public void OnAccountInfoRequestId(CM_ACCOUNT_INFO_REQUEST_ID p)
        {
            SM_ACCOUNT_INFO p1 = new SM_ACCOUNT_INFO();
            p1.SessionID = p.SessionID;

            Account acc = AccountCache.Instance[p.AccountID];
            if (acc != null)
            {
                p1.Result = AccountLoginResult.OK;
                p1.Account = acc;
                Logger.ShowInfo(string.Format("Loading Player:{0}'s account info", acc.UserName));
            }
            else
                p1.Result = AccountLoginResult.NO_SUCH_ACCOUNT;
            this.Network.SendPacket(p1);
        }

        public void OnAccountLogin(CM_ACCOUNT_LOGIN p)
        {
            Logger.ShowInfo(string.Format(this + ":Player:AccountID {0} is trying to login", p.AccountID));
            SM_ACCOUNT_LOGIN_RESULT p1 = new SM_ACCOUNT_LOGIN_RESULT();
            p1.SessionID = p.SessionID;
            p1.Result = AccountManager.Instance.AccountLogin(p.AccountID);
            this.Network.SendPacket(p1);
        }

        public void OnAccountLogout(CM_ACCOUNT_LOGOUT p)
        {
           AccountManager.Instance.AccountLogout(p.AccountID);
        }

        public void OnAccountSave(CM_ACCOUNT_SAVE p)
        {
            AccountManager.Instance.AccountSave(p.Account);
        }
    }
}
