﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SmartEngine.Core;
using SmartEngine.Network;
using SmartEngine.Network.Utils;

using SagaBNS.Common.Packets;
using SagaBNS.Common.Packets.AccountServer;
namespace SagaBNS.AccountServer.Network.Client
{
    public partial class AccountSession : Session<AccountPacketOpcode>
    {
        public bool Authenticated { get; set; }
        static int session = 1;
        int curSession;
        public void OnLoginRequest(Packets.Client.CM_LOGIN_REQUEST p)
        {
            SM_LOGIN_RESULT p2 = new SM_LOGIN_RESULT();
            if (p.Password == Configuration.Instance.Password)
            {
                Logger.ShowInfo(string.Format("Server({0}) successfully authenticated", this.Network.Socket.RemoteEndPoint.ToString()));
                p2.Result = SM_LOGIN_RESULT.Results.OK;
                Authenticated = true;
                curSession = session;
                Interlocked.Increment(ref session);
            }
            else
            {
                Logger.ShowInfo(string.Format("Server({0}) failed authentication with password:{1}", this.Network.Socket.RemoteEndPoint.ToString(), p.Password));
                p2.Result = SM_LOGIN_RESULT.Results.WRONG_PASSWORD;
                Authenticated = false;
            }
            this.Network.SendPacket(p2);
        }
    }
}
