﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SagaBNS.Common.Packets;
using SagaBNS.ChatServer.Network.Client;

namespace SagaBNS.ChatServer.Packets.Client
{
    public class CM_CHANNEL_QUIT : Packet<BNSChatOpcodes>
    {
        public CM_CHANNEL_QUIT()
        {
            this.ID = BNSChatOpcodes.CM_CHANNEL_QUIT;
        }

        public uint ChannelID
        {
            get
            {
                return GetUInt(4);
            }
        }

        public override Packet<BNSChatOpcodes> New()
        {
            return new CM_CHANNEL_QUIT();
        }

        public override void OnProcess(Session<BNSChatOpcodes> client)
        {
            ((ChatSession)client).OnChannelQuit(this);
        }
    }
}
