﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SmartEngine.Network.Utils;
using SagaBNS.Common.Packets;
using SagaBNS.ChatServer.Network.Client;

namespace SagaBNS.ChatServer.Packets.Client
{
    public class CM_PING : Packet<BNSChatOpcodes>
    {
        public CM_PING()
        {
            this.ID = BNSChatOpcodes.CM_PING;
        }

        public override Packet<BNSChatOpcodes> New()
        {
            return new CM_PING();
        }

        public override void OnProcess(Session<BNSChatOpcodes> client)
        {
            ((ChatSession)client).OnPing(this);
        }
    }
}
