﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.ChatServer.Network.Client;

namespace SagaBNS.ChatServer.Packets.Client
{
    public class SM_LOGIN_AUTH_RESULT : Packet<BNSChatOpcodes>
    {
        public SM_LOGIN_AUTH_RESULT()
        {
            this.ID = BNSChatOpcodes.SM_LOGIN_AUTH_RESULT;

            PutInt(1, 2);
            PutShort(4990);
        }
    }
}
