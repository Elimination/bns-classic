﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.ChatServer.Network.Client;

namespace SagaBNS.ChatServer.Packets.Client
{
    public class SM_CHAT : Packet<BNSChatOpcodes>
    {
        public SM_CHAT()
        {
            this.ID = BNSChatOpcodes.SM_CHAT;
        }

        public uint ChannelID
        {
            set
            {
                PutUInt(value, 2);
            }
        }

        public ulong ActorID
        {
            set
            {
                PutULong(value, 6);
            }
        }

        public void PutMessage(string name, string msg)
        {
            PutShort((short)name.Length);
            PutBytes(Encoding.Unicode.GetBytes(name));
            PutShort((short)msg.Length);
            PutBytes(Encoding.Unicode.GetBytes(msg));
        }
    }
}
