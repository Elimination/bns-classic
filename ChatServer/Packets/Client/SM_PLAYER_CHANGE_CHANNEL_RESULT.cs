﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SmartEngine.Network;
using SagaBNS.Common.Packets;
using SagaBNS.ChatServer.Network.Client;

namespace SagaBNS.ChatServer.Packets.Client
{
    public class SM_PLAYER_CHANGE_CHANNEL_RESULT : Packet<BNSChatOpcodes>
    {
        public SM_PLAYER_CHANGE_CHANNEL_RESULT()
        {
            this.ID = BNSChatOpcodes.SM_PLAYER_CHANGE_CHANNEL_RESULT;
        }

        public int Unknown
        {
            set
            {
                PutInt(value, 2);
            }
        }

        public uint ChannelID
        {
            set
            {
                PutUInt(value, 6);
            }
        }

    }
}
